<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav">
    <!--+++++++++++++++++++++++++++++++++++++++ Admin Module  +++++++++++++++++++++++++++++++++++++++-->
    
    <li class="nav-item">
      <a class="nav-link" href="home.php">
        <i class="menu-icon typcn typcn-document-text"></i>
        <span class="menu-title">Dashboard</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="profile.php">
        <i class="menu-icon typcn typcn-document-text"></i>
        <span class="menu-title">Profile</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="user.php">
        <i class="menu-icon typcn typcn-shopping-bag"></i>
        <span class="menu-title">User</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="p_job_register.php">
        <i class="menu-icon typcn typcn-shopping-bag"></i>
        <span class="menu-title">Job Register</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="p_inspections.php">
        <i class="menu-icon typcn typcn-shopping-bag"></i>
        <span class="menu-title">Inspections</span>
      </a>
    </li>

    

    <li class="nav-item">
      <a class="nav-link" href="spare_parts.php">
        <i class="menu-icon typcn typcn-shopping-bag"></i>
        <span class="menu-title">Spare Parts</span>
      </a>
    </li>
    
    <!--+++++++++++++++++++++++++++++++++++++++ Costing Module  +++++++++++++++++++++++++++++++++++++++-->
    
    <li class="nav-item">
      <a
        class="nav-link"
        data-toggle="collapse"
        href="#ui-costing"
        aria-expanded="false"
        aria-controls="ui-costing"
      >
        <i class="menu-icon typcn typcn-coffee"></i>
        <span class="menu-title">Purchase Order</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="ui-costing">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item">
            <a class="nav-link" href="p_purchase_orders.php"
              >New Orders
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="home.php"
              >Order History</a>
          
        </ul>
      </div>
    </li>
    
    <li class="nav-item">
      <a
        class="nav-link"
        data-toggle="collapse"
        href="#ui-bom"
        aria-expanded="false"
        aria-controls="ui-bom"
      >
        <i class="menu-icon typcn typcn-coffee"></i>
        <span class="menu-title">Payments</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="ui-bom">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item">
            <a class="nav-link" href="p_payment.php">Make payment</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="p_payment.php">Payment History</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="p_payment.php">Payments</a>
          </li>
        </ul>
      </div>
    </li>
    <!--+++++++++++++++++++++++++++++++++++++++ Reports Module  +++++++++++++++++++++++++++++++++++++++-->
    <!-- <li class="nav-item nav-category">Reports</li> -->
    <li class="nav-item">
      <a
        class="nav-link"
        data-toggle="collapse"
        href="#ui-reports"
        aria-expanded="false"
        aria-controls="ui-reports"
      >
        <i class="menu-icon typcn typcn-coffee"></i>
        <span class="menu-title">Reports</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="ui-reports">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item">
            <a class="nav-link" href="home.php"
              >Job Status Report</a
            >
          </li>
          <li class="nav-item">
            <a class="nav-link" href="home.php">P/O Report</a>
          </li>
        </ul>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="home.php">
        <i class="menu-icon typcn typcn-shopping-bag"></i>
        <span class="menu-title">Settings</span>
      </a>
    </li>
  </ul>
</nav>

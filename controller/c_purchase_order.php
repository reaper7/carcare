<?php
  // Database Connection
    require "../include/config.php";
  // get spareparts for the po
  if (isset($_GET["poSpareparts"])) {
    $jId = $_GET["jId"];
    $data["state"] = 1;
    $dataArr = [];
    $res = mysqli_query(
      $conn,
      "SELECT DISTINCT p.po_id,s.spNo,s.spName,j.jobId,p.poDate,p.poTime,js.orderedQuantity,s.spPrice FROM purchase_order p INNER JOIN job j ON p.job_id = j.jobId INNER JOIN job_sparepart js ON js.job_id = j.jobId INNER JOIN spareparts s ON js.sparepartid = s.spId WHERE j.jobId =" .
        $jId .
        ""
    );
    if (mysqli_num_rows($res) > 0) {
      while ($row = mysqli_fetch_assoc($res)) {
        array_push($dataArr, $row);
      }
      // output data of each row
      $data["data"] = $dataArr;
      $data["state"] = 1;
    } else {
      $data["state"] = 0;
    }
    header("Content-type: application/json; charset=utf-8"); //inform the browser we're sending JSON data
    echo json_encode($data); //echoing JSON encoded data as the response for the AJAX call
  }
?>
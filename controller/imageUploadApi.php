<?php
include 'connection.php';
    /* Getting file name */
   $filename = $_FILES['file']['name'];
   /* Location */
   $location = "uploads/".$filename;
   $uploadOk = 1;
   $imageFileType = pathinfo($location,PATHINFO_EXTENSION);
   /* Valid Extensions */
   $valid_extensions = array("jpg","jpeg","png");
   /* Check file extension */
   if( !in_array(strtolower($imageFileType),$valid_extensions) ) {
      $uploadOk = 0;
   }
   if($uploadOk == 0){
      echo 0;
   }else{
      /* Upload file */
      if(move_uploaded_file($_FILES['file']['tmp_name'],$location)){
         $data['state'] = 1;
         $data['image'] = $location;
         header("Content-type: application/json; charset=utf-8"); 
         echo json_encode($data);
      }else{
         echo 0;
      }
   }
?>
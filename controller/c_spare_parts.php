<?php
// Database Connection
require "../include/config.php";
// sparepart Add Function
if (isset($_POST["add_sp"]) && $_POST["add_sp"] == "add_sp") {
  // $spId = $row['spId'];
  $spNo = $_POST["spNo"];
  $spName = $_POST["spName"];
  $spCategory = $_POST["spCategory"];
  $spCountry = $_POST["spCountry"];
  $spMade = $_POST["spMade"];
  $spQuantity = $_POST["spQuantity"];
  $spCost = $_POST["spCost"];
  $spPrice = $_POST["spPrice"];
  $spProfit = $_POST["spProfit"];
  $spDate = $_POST["spDate"];
  $insertPart = "INSERT INTO spareparts (spNo,spName,spCategory,spCountry,spMade,spQuantity,spCost,spPrice,spProfit,spDate) VALUES ('$spNo','$spName','$spCategory','$spCountry','$spMade','$spQuantity','$spCost','$spPrice','$spProfit','$spDate')";
  // $insert = "INSERT INTO spareparts (spId,spName,spCategory,spCountry,spMade,spQuantity,spCost,spPrice,spProfit,spDate) VALUES
  // ('$spId','$spName','$spCategory','$spCountry','$spMade','$spQuantity','$spCost','$spPrice','$spProfit','$spDate')";
  $result = mysqli_query($conn, $insertPart);
  if ($result) {
    echo 1;
  } else {
    echo mysqli_error($conn);
  }
}
// get spareparts for the inspection
if (isset($_GET["get_spareparts"])) {
  if ($_GET["get_spareparts"] = "get_spareparts") {
    $data["state"] = 1;
    $dataArr = [];
    $res = mysqli_query($conn, "SELECT * FROM spareparts");
    if (mysqli_num_rows($res) > 0) {
      while ($row = mysqli_fetch_assoc($res)) {
        array_push($dataArr, $row);
      }
      // output data of each row
      $data["data"] = $dataArr;
      $data["state"] = 1;
    } else {
      $data["state"] = 0;
    }
    header("Content-type: application/json; charset=utf-8"); //inform the browser we're sending JSON data
    echo json_encode($data); //echoing JSON encoded data as the response for the AJAX call
  }
}
//update sparepart
if (isset($_POST["add_sp"]) && $_POST["add_sp"] == "update") {
  $spId = $_POST["sparepartId"];
  $spCategory = $_POST["spCategory"];
  $spCost = $_POST["spCost"];
  $spCountry = $_POST["spCountry"];
  $spName = $_POST["spName"];

  $spMade = $_POST["spMade"];
  $spNo = $_POST["spNo"];
  $spPrice = $_POST["spPrice"];
  $spProfit = $_POST["spProfit"];
  $spQuantity = $_POST["spQuantity"];

  // spareparts
  $updateSpare =
    "UPDATE spareparts SET spNo='$spNo', spName='$spName',
                spCategory='$spCategory',spMade='$spMade',spCountry='$spCountry',
                spQuantity='$spQuantity',spCost='$spCost',spPrice='$spPrice',spProfit='$spProfit'  WHERE spId ='" .
    $spId .
    "'";

  $resultSpare = mysqli_query($conn, $updateSpare);
  if ($resultSpare) {
    // vehicle
    echo 1;
  } else {
    echo mysqli_error($conn);
  }
}
//delete spareparts in inspections
if (isset($_POST["act"])) {
  if ($_POST["act"] == "delete_sparepart") {
    $spId = $_POST["spId"];
    $delData =
      "DELETE FROM spareparts WHERE spId='" .
      $spId ."'";
    $resultData = mysqli_query($conn, $delData);
    if ($resultData) {
      echo 1;
    } else {
      echo mysqli_error($conn);
    }
  }
}
?>

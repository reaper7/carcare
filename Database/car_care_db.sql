-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 17, 2021 at 06:32 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `car_care_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `cliant`
--

CREATE TABLE `cliant` (
  `cNic` varchar(250) NOT NULL,
  `cName` varchar(250) NOT NULL,
  `cEmail` varchar(250) NOT NULL,
  `cAddress` varchar(250) NOT NULL,
  `cTel` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cliant`
--

INSERT INTO `cliant` (`cNic`, `cName`, `cEmail`, `cAddress`, `cTel`) VALUES
('12345566', 'bear s123', 'beat123@gmail.com', '\"shashi prabha123\"\r\nHabaragoda watte, karagaswagura', '071995112'),
('45454545v', 'dilnuwan', 'dpb.prasad@gmail.com', 'Karawilahena Road,\r\n408/1', '+94773848288'),
('5822990v', 'john deer', 'deer@gmail.com', '\"shashi prabha\"\r\nHabaragoda watte, karagaswagura', '0719951634'),
('858555522v', 'Danushka Prasad', 'dpb.prasad@gmail.com', 'Karawilahena Road,\r\n408/1', '+94773848288'),
('9201548245', 'lobster', 'lob@gmail.com', '\"shashi prabha\"\r\nHabaragoda watte, karagaswagura', '0719951634'),
('92080255v', 'load beat', 'loads@gmail.com', '174/F\r\nGalahitiyawa', '0719951634'),
('92081245v', 'jeseph madela', 'dere@gmail.com', '\"shashi prabha\"\r\nHabaragoda watte, karagaswagura', '0719951634'),
('920898888v', 'kelum Prasad', 'dpb.prasad@gmail.com', 'Karawilahena Road,\r\n408/1', '+94773848288');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `owner` varchar(50) NOT NULL,
  `address` int(150) NOT NULL,
  `tp_no` varchar(30) NOT NULL,
  `logo` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `job`
--

CREATE TABLE `job` (
  `jobId` int(250) NOT NULL,
  `vNo` varchar(10) NOT NULL,
  `cDescription` varchar(250) NOT NULL,
  `receiveDate` varchar(250) NOT NULL,
  `receiveTime` varchar(250) NOT NULL,
  `technicalDescription` varchar(250) NOT NULL,
  `jobType` varchar(250) NOT NULL,
  `jstatus` varchar(10) NOT NULL,
  `deliverTime` varchar(10) NOT NULL,
  `jobAssign` varchar(20) NOT NULL,
  `proof` varchar(10) NOT NULL,
  `reason` varchar(200) NOT NULL,
  `paid_status` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job`
--

INSERT INTO `job` (`jobId`, `vNo`, `cDescription`, `receiveDate`, `receiveTime`, `technicalDescription`, `jobType`, `jstatus`, `deliverTime`, `jobAssign`, `proof`, `reason`, `paid_status`) VALUES
(4, 'BHFFFF', 'dasd', '04/09/2021', '16:17', 'ac service', 'Inspection', 'Completed', '', 'AC', 'Yes', '', 1),
(5, 'BHDDDDD', 'sdasd normal', '04/09/2021', '16:18', 'engine repair', 'Inspection', 'Completed', '', 'Mechanic', 'Yes', '', 1),
(6, 'BFF25555', 'fsdf', '04/09/2021', '16:52', 'ssss', 'Inspection', 'Pending', '', 'Mechanic', 'Yes', '', 0),
(7, 'MV2525', 'engine service', '04/19/2021', '20:39', '', 'Inspection', 'Pending', '', 'Electrician', 'Yes', '', 0),
(8, 'BV1245', 'leaking oil 123', '05/09/2021', '16:33', 'gear break', 'Service', 'Processing', '12', 'Mechanic', 'Yes', '', 0),
(9, 'BH2445', 'low break', '05/13/2021', '12:02', '', 'Inspection', 'Pending', '', '', '', '', 0),
(10, 'BH2458', 'clutch fail', '05/13/2021', '12:06', 'low fluids', 'Inspection', 'Completed', '', 'Mechanic', 'Yes', '', 1),
(11, 'CBC2875', 'engine fail', '05/16/2021', '14:55', '', 'Service', 'Pending', 'null', 'null', 'null', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `job_sparepart`
--

CREATE TABLE `job_sparepart` (
  `job_id` int(11) NOT NULL,
  `sparepartid` int(11) NOT NULL,
  `orderedQuantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `job_sparepart`
--

INSERT INTO `job_sparepart` (`job_id`, `sparepartid`, `orderedQuantity`) VALUES
(4, 103, 1),
(5, 104, 3),
(6, 101, 2),
(6, 104, 2),
(7, 101, 3),
(7, 103, 1),
(7, 104, 2),
(8, 103, 2),
(9, 104, 3),
(10, 101, 2);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `pay_id` int(10) NOT NULL,
  `job_id` int(10) NOT NULL,
  `date_paid` varchar(10) NOT NULL DEFAULT current_timestamp(),
  `paid_status` int(1) NOT NULL DEFAULT 0,
  `payment_type` varchar(20) NOT NULL,
  `total_amount` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`pay_id`, `job_id`, `date_paid`, `paid_status`, `payment_type`, `total_amount`) VALUES
(12, 4, '2021-05-12', 1, 'Cash', 48012),
(13, 10, '2021-05-13', 1, 'Cash', 2300);

-- --------------------------------------------------------

--
-- Table structure for table `payment_detail`
--

CREATE TABLE `payment_detail` (
  `pay_detail_id` int(10) NOT NULL,
  `pay_id` int(10) NOT NULL,
  `pay_description` varchar(50) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payment_detail`
--

INSERT INTO `payment_detail` (`pay_detail_id`, `pay_id`, `pay_description`, `amount`) VALUES
(3, 12, 'as', 12),
(4, 13, 'service breaks', 1200);

-- --------------------------------------------------------

--
-- Table structure for table `po_sparepart`
--

CREATE TABLE `po_sparepart` (
  `po_id` int(10) NOT NULL,
  `spid` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `po_sparepart`
--

INSERT INTO `po_sparepart` (`po_id`, `spid`) VALUES
(14, 103),
(14, 104),
(15, 103),
(15, 104),
(16, 103),
(16, 104),
(17, 103),
(17, 104),
(18, 103),
(19, 104);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order`
--

CREATE TABLE `purchase_order` (
  `po_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `poTime` varchar(10) NOT NULL,
  `poDate` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchase_order`
--

INSERT INTO `purchase_order` (`po_id`, `job_id`, `poTime`, `poDate`) VALUES
(14, 5, '02:46:', '2021/05/09'),
(15, 7, '19:51:', '2021/05/09'),
(16, 6, '20:13:', '2021/05/09'),
(17, 4, '22:35:', '2021/05/12'),
(18, 8, '00:21:', '2021/05/13'),
(19, 9, '04:56:', '2021/05/15'),
(20, 4, '20:56:', '2021/05/15');

-- --------------------------------------------------------

--
-- Table structure for table `spareparts`
--

CREATE TABLE `spareparts` (
  `spId` int(250) NOT NULL,
  `spNo` varchar(20) NOT NULL,
  `spName` varchar(250) NOT NULL,
  `spCategory` varchar(250) NOT NULL,
  `spCountry` varchar(250) NOT NULL,
  `spMade` varchar(250) NOT NULL,
  `spQuantity` varchar(250) NOT NULL,
  `spCost` varchar(250) NOT NULL,
  `spPrice` varchar(250) NOT NULL,
  `spProfit` varchar(250) NOT NULL,
  `spDate` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spareparts`
--

INSERT INTO `spareparts` (`spId`, `spNo`, `spName`, `spCategory`, `spCountry`, `spMade`, `spQuantity`, `spCost`, `spPrice`, `spProfit`, `spDate`) VALUES
(101, 'P12121', 'Mobile Oil 2L', 'Oil', 'None', 'Mobile', '25', '450', '550', '100', '04/09/2021'),
(102, 'P21345', 'ss', 'SuperUser', 'S1', 'S1', '12', '1222', '1222', '12', ''),
(103, 'P14512', 'Ac Filter', 'Moderator', 'S1', 'S1', '0', '784', '1000', '', ''),
(104, 'P895475', 'Dello 500', 'Admin', 'S1', 'S2', '0', '3200', '4000', '1200', '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `employeeName` varchar(250) NOT NULL,
  `employeeId` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `section` varchar(250) NOT NULL,
  `userLevel` varchar(250) NOT NULL,
  `createdBy` varchar(250) NOT NULL,
  `cdate` varchar(250) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(10000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `employeeName`, `employeeId`, `email`, `section`, `userLevel`, `createdBy`, `cdate`, `username`, `password`) VALUES
(6, 'Kamal', '', 'kamal@gmail.com', 'S1', '', '', '', 'kamal', 'f5365ca2098270ec682906de5118e8ee'),
(7, 'Nimal', '', 'nimal@gmail.com', 'S2', '', '', '', 'nimal', '9ad24f2850062550f982603578e32db6'),
(8, 'Danushka', '', 'dpb.prasad@gmail.com', 'S3', '', '', '', 'danushka', '785fcfdd1546ad8d3d5b603ae7d87300'),
(9, 'Asanka', '456', 'asanka@gmail.com', 'S1', 'B3', '', '', 'asanka', 'd0c9a191d670c09e9ad0206b45516890'),
(10, 'Kamal', '456', 'dpb.prasad@gmail.com', 'S1', 'Admin', 'kamal', '12/03/2021', 'kamal', 'd0c9a191d670c09e9ad0206b45516890'),
(11, 'admin', '4512', 'kap@gmail.com', 'S1', 'Admin', '', '', 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle`
--

CREATE TABLE `vehicle` (
  `vNo` varchar(10) NOT NULL,
  `ownerNic` varchar(250) NOT NULL,
  `brandName` varchar(250) NOT NULL,
  `modelYear` varchar(250) NOT NULL,
  `bodyType` varchar(250) NOT NULL,
  `odometer` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle`
--

INSERT INTO `vehicle` (`vNo`, `ownerNic`, `brandName`, `modelYear`, `bodyType`, `odometer`) VALUES
('BFF25555', '45454545v', 'BMW', '2012', 'Car', 0),
('BH2445', '92081245v', 'Honda', '2020', 'Car', 25000),
('BH2458', '5822990v', 'Honda', '2020', 'Car', 25000),
('BHDDDDD', '920898888v', 'BMW', '2015', 'Car', 0),
('BHFFFF', '858555522v', 'BMW', '2015', 'Car', 0),
('BV1245', '12345566', 'Toyota', '2019', 'SUV', 65600),
('CBC2875', '92080255v', 'Honda', '2019', 'Double Cab', 28590),
('MV2525', '9201548245', 'Audi', '2015', 'SUV', 21000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cliant`
--
ALTER TABLE `cliant`
  ADD PRIMARY KEY (`cNic`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job`
--
ALTER TABLE `job`
  ADD PRIMARY KEY (`jobId`),
  ADD KEY `vNo` (`vNo`);

--
-- Indexes for table `job_sparepart`
--
ALTER TABLE `job_sparepart`
  ADD PRIMARY KEY (`job_id`,`sparepartid`),
  ADD KEY `sparepartid` (`sparepartid`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`pay_id`),
  ADD KEY `job_id` (`job_id`);

--
-- Indexes for table `payment_detail`
--
ALTER TABLE `payment_detail`
  ADD PRIMARY KEY (`pay_detail_id`),
  ADD KEY `payment_detail_ibfk_1` (`pay_id`);

--
-- Indexes for table `po_sparepart`
--
ALTER TABLE `po_sparepart`
  ADD PRIMARY KEY (`po_id`,`spid`),
  ADD KEY `spid` (`spid`);

--
-- Indexes for table `purchase_order`
--
ALTER TABLE `purchase_order`
  ADD PRIMARY KEY (`po_id`),
  ADD KEY `job_id` (`job_id`);

--
-- Indexes for table `spareparts`
--
ALTER TABLE `spareparts`
  ADD PRIMARY KEY (`spId`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle`
--
ALTER TABLE `vehicle`
  ADD PRIMARY KEY (`vNo`),
  ADD KEY `vehicle_ibfk_1` (`ownerNic`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `job`
--
ALTER TABLE `job`
  MODIFY `jobId` int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `pay_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `payment_detail`
--
ALTER TABLE `payment_detail`
  MODIFY `pay_detail_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `purchase_order`
--
ALTER TABLE `purchase_order`
  MODIFY `po_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `spareparts`
--
ALTER TABLE `spareparts`
  MODIFY `spId` int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `job`
--
ALTER TABLE `job`
  ADD CONSTRAINT `job_ibfk_1` FOREIGN KEY (`vNo`) REFERENCES `vehicle` (`vNo`);

--
-- Constraints for table `job_sparepart`
--
ALTER TABLE `job_sparepart`
  ADD CONSTRAINT `job_sparepart_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `job` (`jobId`),
  ADD CONSTRAINT `job_sparepart_ibfk_2` FOREIGN KEY (`sparepartid`) REFERENCES `spareparts` (`spId`);

--
-- Constraints for table `payment`
--
ALTER TABLE `payment`
  ADD CONSTRAINT `payment_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `job` (`jobId`);

--
-- Constraints for table `payment_detail`
--
ALTER TABLE `payment_detail`
  ADD CONSTRAINT `payment_detail_ibfk_1` FOREIGN KEY (`pay_id`) REFERENCES `payment` (`pay_id`);

--
-- Constraints for table `po_sparepart`
--
ALTER TABLE `po_sparepart`
  ADD CONSTRAINT `po_sparepart_ibfk_2` FOREIGN KEY (`spid`) REFERENCES `spareparts` (`spId`);

--
-- Constraints for table `purchase_order`
--
ALTER TABLE `purchase_order`
  ADD CONSTRAINT `purchase_order_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `job` (`jobId`);

--
-- Constraints for table `vehicle`
--
ALTER TABLE `vehicle`
  ADD CONSTRAINT `vehicle_ibfk_1` FOREIGN KEY (`ownerNic`) REFERENCES `cliant` (`cNic`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

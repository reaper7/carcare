<!DOCTYPE html>
<html lang="en">
  <?php require "../include/config.php"; ?>
  <!-- include head code here -->
  <?php include "../include/head.php"; ?>
  <body>
    <div class="container-scroller">
      <!-- partial:../../partials/_navbar.html -->
      <!-- include nav code here -->
      <?php include "../include/nav.php"; ?>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:../../partials/_sidebar.html -->
        <!-- include sidebar code here -->
        <?php include "../include/sidebar.php"; ?>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">
              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                  <h4 class="card-title">Payments</h4>
                    <p class="card-description fl">Dashboard >> <code>Make Payment</code> </p>
                    <button type="submit" class="btn btn-primary btn-fw fr"data-toggle="modal" data-target="#myModal" onclick="setDate()" >Create New Job</button>              
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th> # </th>
                          <th>Customer</th>
                          <th>Telephone</th>
                          <th>J.Id</th>
                          <th>V/No</th>
                          <th>V/Brand</th>
                          <th>J.Status</th>
                          <th>Job</th>
                          <th>P.Status</th>
                          <th>Action</th>
                        </tr>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $selJid = 0;
                        $sql = mysqli_query(
                          $conn,
                          "SELECT * FROM cliant c 
                            INNER JOIN vehicle v ON c.cNic = v.ownerNic
                            INNER JOIN job j ON v.vNo = j.vNo
                            WHERE j.jobType='Inspection' && j.jstatus='Completed'
                            ORDER BY c.cName"
                        );

                        $numRows = mysqli_num_rows($sql);
                        $jobArray = [];
                        if ($numRows > 0) {
                          $i = 1;
                          while ($row = mysqli_fetch_assoc($sql)) {
                            $cName = $row["cName"];
                            $customerEmail = $row["cEmail"];
                            $telephone = $row["cTel"];
                            $cAddress = $row["cAddress"];
                            $vNo = $row["vNo"];
                            $brandName = $row["brandName"];
                            $cDescription = $row["cDescription"];
                            $jobType = $row["jobType"];
                            $jstatus = $row["jstatus"];
                            $jobId = $row["jobId"];
                            $paid_status = $row["paid_status"];

                            array_push(
                              $jobArray,
                              (object) [
                                "jobId" => $jobId,
                                "vNo" => $vNo,
                                "ownerNic" => $row["ownerNic"],
                                "cName" => $cName,
                                "cDescription" => $cDescription,
                                "jobType" => $jobType,
                                "cEmail" => $customerEmail,
                                "cTel" => $telephone,
                                "cAddress" => $cAddress,
                                "brandName" => $brandName,
                                "jstatus" => $row["jstatus"],
                                "deliverTime" => $row["deliverTime"],
                                "jobAssign" => $row["jobAssign"],
                                "reason" => $row["reason"],
                                "ownerNic" => $row["ownerNic"],
                                "modelYear" => $row["modelYear"],
                                "bodyType" => $row["bodyType"],
                                "odometer" => $row["odometer"],
                                "receiveDate" => $row["receiveDate"]
                              ]
                            );
                            echo " <tr>";
                            echo " <td>" . $i . " </td>";
                            echo " <td>" . $cName . " </td>";
                            echo " <td>" . $telephone . " </td>";
                            echo " <td>" . $jobId . " </td>";
                            echo " <td>" . $vNo . " </td>";
                            echo " <td>" . $brandName . " </td>";
                            echo " <td>" . $jstatus . " </td>";
                            echo " <td>" . $jobType . " </td>";
                             if($paid_status == 1){ echo " <td>Paid</td>";} else{echo " <td>Not Paid</td>";}
                            echo " <td>";
                            echo '<button onclick="openPayInfo(' .
                              $jobId .
                              ')" style="width: 16px;height: 27px;margin-right: 10px;"' .
                              'class="btn btn-primary"' .
                              'data-toggle="modal"' .
                              'data-target="#myModal"><i class="fas fa-file-invoice-dollar" style="margin-right: 10px !important;margin: -4px;"></i></button>';
                            echo '<button onclick="viewRow(' .
                              $jobId .
                              ')" style="width: 16px;height: 27px;"' .
                              'class="btn btn-success"' .
                              'data-toggle="modal"' .
                              'data-target="#myModal"><i class="fa fa-play" style="margin-right: 10px !important;margin: -4px;"></i></button>';
                            echo " </td>";
                            echo " </tr>";
                            $i++;
                          }
                        }
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
             
             
            </div>
          </div>
          <!-- content-wrapper ends -->
          <!-- partial:../../partials/_footer.html -->
          <!-- include footer coe here -->
          <?php include "../include/footer.php"; ?>
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- include footer coe here -->
    <?php include "../include/footer-js.php"; ?>

  </body>
</html>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog modal-lg">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <h4 class="modal-title" id="titleJob">Job</h4>
      </div>
      <div class="modal-body" >
        
        <div class="card-body" id="job">
          <form class="form-sample" id="jobRegister">
            <label class="card-title text-danger" id="label_id" ></label>

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Name</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="cName" placeholder ="Enter Customer Name"/>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Brand</label>
                    <div class="col-sm-9">
                      <select id="brandName" class="form-control">
                        <option>-Select Brand-</option>
                        <option value="BMW">BMW</option>
                        <option value="Audi">Audi</option>
                        <option value="Honda">Honda</option>
                        <option value="Toyota">Toyota</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Email</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="cEmail" placeholder ="Enter Email"/>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Body Type</label>
                    <div class="col-sm-9">
                    <select id="bodyType" class="form-control">
                        <option>-Select Type-</option>
                        <option value="Car">Car</option>
                        <option value="Double Cab">Double Cab</option>
                        <option value="SUV">SUV</option>
                        <option value="Truck">Truck</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Address</label>
                    <div class="col-sm-9">
                      <!-- <input type="text" class="form-control" name="address" placeholder ="Enter Address"/> -->
                      <textarea name="cAddress" class="form-control" id="cAddress" rows="4" placeholder="Enter Address"></textarea>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Description</label>
                    <div class="col-sm-9">
                      <!-- <input type="text" class="form-control" name="description" placeholder ="Enter Description"/> -->
                      <textarea class="form-control" name="cDescription" id="cDescription" rows="4" placeholder="Description"></textarea>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Telephone</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="cTel" placeholder ="Enter Telephone"/>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Year</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="modelYear" placeholder ="Enter Model Year"/>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Vehicle No</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="vNo" name="vNo" placeholder ="Enter V/No"/>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Date</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" Blocked id="receiveDate" name="receiveDate" placeholder ="dd/mm/yyyy"/>
                      <input type="hidden" Blocked class="form-control" id="receiveTime" name="receiveTime"/>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label" >NIC</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="cNic" placeholder ="Enter Customer NIC"/>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Odometer</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="odometer" placeholder ="Enter Odometer Reading"/>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Next</label>
                    <div class="col-sm-9">
                      <select id="jobType" class="form-control">
                        <option value="Inspection">Inspection</option>
                        <option value="Parts">Parts</option>
                        <option value="P/O">P/O</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>

              <input type="hidden" class="form-control" id="post_job" value="post_job" />
              <button type="submit" id="submitBtn" class="btn btn-success mr-2 fr" >Submit</button>      
          </form>
          
        </div>
        <div class="card-body" id="payment" style="display:none;">
          <div class="row" style="display: flex; justify-content: space-between;">
            <label class="card-title" id="label_id2" ></label>
            <label class="card-title text-danger" id="label_total" ></label>
          </div>
          <form class="form-sample" id="jobPay">
              <!-- <p class="card-description">Job id</p> -->

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Payment Type</label>
                    <div class="col-sm-9">
                      <select id="paymentType" name="paymentType" class="form-control">
                        <option>-Select Payment Method-</option>
                        <option value="Cash">Cash</option>
                        <option value="Card">Card</option>
                      </select>
                    </div>
                  </div>
                  
                </div>
                <div class="col-md-6">
                 
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Description</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" name="amount" id="desAmount" placeholder ="Enter Description"/>
                      <!-- <textarea class="form-control" name="note" id="desAmount"  placeholder="Note"></textarea> -->
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Amount</label>
                    <div class="col-sm-9">
                      <input type="number" class="form-control" id="amount" name="amount" placeholder ="Enter amount"/>
                    </div>
                  </div>
                </div>
                <div class="col-md-2">
                  <button type="button" onclick="addPayment(event)" class="btn btn-success mr-2 fr" style="border-radius: 62%;
                      height: 30px;
                      width: 30px;">
                      <i class="fas fa-plus" style="margin-left: -5px;"></i>
                  </button> 
                </div>
              </div>
              <div class="row mt-4">
                  <div class="col-md-8">
                    <label>Other Payments</label>
                    <label id="otherPaymentsTotal"></label>
                    <table id="paymentTable" class="table table-hover" >
                      <thead>
                          <th>Description</th>
                          <th>Amount</th>
                          <th>Action</th>
                      </thead>
                      <tbody id="tbodyid">
                      </tbody>
                    </table>
                  </div>
                </div>
              <div class="row mt-4">
                  <div class="col-md-12">
                    <label>Spare parts Ordered</label>
                    <table id="partTable" class="table table-hover" >
                      <thead>
                          <th>Part Name</th>
                          <th>Part No</th>
                          <th>Status</th>
                          <th>Quantity</th>
                          <th>U.Price</th>
                          <th style="text-align: end;" >S.total</th>
                      </thead>
                      <tbody id="tbodyid">
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="col-md-12 mt-4">
                  <input type="hidden" class="form-control" name="post_job" value="post_job" />
                  <button type="button" onclick="submitPayment()" class="btn btn-success mr-2 fr" >Submit</button>      
                </div>
          </form>
          
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    
  </div>
</div>

  <script>
    var jobArray = <?php echo json_encode($jobArray);?>;
    var selJid= 0;
    var totalBillParts=0;
    var otherPayments=0;
    var payment ={
      id:"",
      description : "",
      amount: ""
    };
    var count=0;
    var payments=[];
    var selectedSpareparts = [];
    var addedSpareparts = [];
    var totalBill=0;
    var jobId="";
    function submitPayment() {
      var paymentType=$("#paymentType").val();
      var spart = selectedSpareparts;
      var payment = JSON.stringify([{
                  'jobId' : jobId,
                  'total_amount': totalBill,
                  'payment_type': paymentType
                }]);
      $.ajax({
        type: "post",
        url: "../controller/c_payment.php",
        data:{ payment_data: payment, payment_detail: payments,form:"postPayment" },
        success: function (data) {
          swal({
            title: "Good job !",
            text: "Successfully Submited",
            icon: "success",
            button: "Ok !",
          });
          // setTimeout(function () {
          //   location.reload();
          // }, 2500);
        },
      });
    }
    function viewRow(jid) {
      $('#payment').hide();
      $('#job').show();
      $('#label_id').text("Job id: "+jid);
      $('#titleJob').text("Job Info");
      setData(jid);
    }
    function setData(id) {
      $( "#vNo" ).prop( "disabled", true );
      $( "#cNic" ).prop( "disabled", true );
      $( "#receiveDate" ).prop( "disabled", true );
      $( "#submitBtn" ).prop( "disabled", true );
      $("#post_job").val("edit");
      $("#update_job").val(id);
      console.log(jobArray);
      var selectedJob={}
      jobArray.forEach(element => {
        if(id==element.jobId){
          selectedJob=element;
        }
      });
      $("#vNo").val(selectedJob.vNo);
      $("#cName").val(selectedJob.cName);
      $("#cDescription").val(selectedJob.cDescription);
      $("#jobType").val(selectedJob.jobType);
      $("#cEmail").val(selectedJob.cEmail);
      $("#cTel").val(selectedJob.cTel);
      $("#cAddress").val(selectedJob.cAddress);
      $("#brandName").val(selectedJob.brandName);
      $("#jobAssign").val(selectedJob.jobAssign);
      $("#reason").val(selectedJob.reason);
      $("#cNic").val(selectedJob.ownerNic);
      $("#modelYear").val(selectedJob.modelYear);
      $("#bodyType").val(selectedJob.bodyType);
      $("#odometer").val(selectedJob.odometer);
      $("#receiveDate").val(selectedJob.receiveDate);
    }
    function openPayInfo(jid) {
      $('#job').hide();
      $('#payment').show();
      $('#label_id2').text("Job id: "+jid);
      $('#titleJob').text("Payment Info");
      selectRow(jid);
    }
    //set jobid value 
  function selectRow(val) {
    clearTable();
    jobId=val;
    var resData = [];
    console.log("data");
    console.log(val);
    // document.getElementById("jobId").value = val;
     $('#jobId').val(val);
      var id = val;
      $.get(
          "../controller/c_inspections.php", {
              sparepartlist: "sparepartlist",
              id : id
          },
          function(data) {
              resData = data.data;
              if(resData!=undefined){
                if(resData.length>0){
                  setSparepart(resData);
                }
              }
        }
      );
    var jobArray = JSON.parse('<?php echo json_encode($jobArray); ?>');
    console.log(jobArray);
    var selectedJob={}
    jobArray.forEach(element => {
      if(val==element.jobId){
        selectedJob=element;
      }
    });
    $("#jobAssign").val(selectedJob.jobAssign);
    $("#technicalDescription").val(selectedJob.technicalDescription);
    $("#proof").val(selectedJob.proof);
    $("#statusDropdown").val(selectedJob.jstatus);
  }
  function setSparepart(spareparts) {
    selectedSpareparts=spareparts;
    
    var table = document.getElementById("partTable");
    totalBillParts=0;
    spareparts.forEach(element => {
      var partName = element.spName;
      var spNo = element.spNo;
      var spQuantity = parseInt(element.spQuantity);
      var sTotal = parseInt(element.orderedQuantity)*parseInt(element.spPrice);
      var status = spQuantity>=parseInt(element.orderedQuantity) ? "Avialable" : "Out of stock";
      totalBillParts = totalBillParts+sTotal;
      
      console.log(totalBill);
      var row = table.insertRow(-1);
      var cell1 = row.insertCell(0);
      var cell2 = row.insertCell(1);
      var cell3 = row.insertCell(2);
      var cell4 = row.insertCell(3);
      var cell5 = row.insertCell(4);
      var cell6 = row.insertCell(5);
      var cell7 = row.insertCell(6);
      
      cell1.innerHTML = partName;
      cell2.innerHTML = spNo;
      cell3.innerHTML = status;
      cell4.innerHTML = element.orderedQuantity;
      cell5.innerHTML = element.spPrice;
      cell6.innerHTML = sTotal;
      // cell4.innerHTML = "<div><input style="+"'width:50px;'"+ "class="+"'form-control'"+"placeholder='0'"+
      //                   "autocomplete="+"'off'"+"/>"+"</div>";
      cell7.innerHTML = "";
    });
    totalBill = totalBillParts;
    $("#label_total").text("Total: "+totalBill);
    // $("#total").val()=totalBillParts;
    var row2 = table.insertRow(-1);
    var r2cell0 = row2.insertCell(0);
    var r2cell1 = row2.insertCell(1);
    var r2cell2 = row2.insertCell(2);
    var r2cell3 = row2.insertCell(3);
    var r2cell4 = row2.insertCell(4);
    var r2cell5 = row2.insertCell(5);
    var r2cell6 = row2.insertCell(6);
    r2cell0.innerHTML = "Total";
    r2cell5.innerHTML = totalBillParts;
    $("#total").text("Total: "+totalBillParts);
  }
  function clearTable(){
    totalBillParts=0;
    selectedSpareparts = [];
    addedSpareparts= [];
    $("#total").text("Total: "+totalBillParts);
    removeTableBody("partTable");
  }
  function removeTableBody(id){
    var tableHeaderRowCount = 1;
    var table = document.getElementById(id);
    var rowCount = table.rows.length;
    for (var i = tableHeaderRowCount; i < rowCount; i++) {
        table.deleteRow(tableHeaderRowCount);
    }
  }
  function addPayment(e) {
    var description = $('#desAmount').val();
    var amount = $('#amount').val();
    if(description=="" || description==undefined){
        swal({
        title: "Please enter payment description !",
        text: "Description",
        icon: "error",
        button: "Ok !",
      });
      return;
    }
    if(amount=="" || amount==undefined){
      swal({
        title: "Please enter payment amount !",
        text: "Amount",
        icon: "error",
        button: "Ok !",
      });
      return;
    }
    payment = {
      id: count,
      description : description,
      amount: amount
    }
    if(payments.length>0){
      var paymentTrue= false;
      payments.forEach(element => {
        if(element.description!= description){
          paymentTrue = true;
        }else{
          paymentTrue = false;
        }
      });
      if(paymentTrue){
        count=count + 1;
        payment = {
          id: count,
          description : description,
          amount: amount
        }
        payments.push(payment);
        otherPayments = otherPayments+parseInt(amount);
        totalBill = totalBill + otherPayments;
      }
      }else{
        payment = {
          id: count,
          description : description,
          amount: amount
        }
        count=count + 1;
        payments.push(payment);
        otherPayments = otherPayments+parseInt(amount);
        totalBill = totalBill + otherPayments;
    }
    
    this.removeTableBody("paymentTable");
    var table = document.getElementById("paymentTable");
    payments.forEach(element => {
        
        
        var row = table.insertRow(-1);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        

        cell1.innerHTML = element.description;
        cell2.innerHTML = element.amount;
        
        cell3.innerHTML = "<button type="+"'button'"+"class="+"'btn btn-danger mr-2 fr'"+ "value="+"'"+element.id+"'"+
                          "onclick="+"'"+"removeItem(this)"+"'"+">Delete </button>";
    });
    var row2 = table.insertRow(-1);
    var r2cell0 = row2.insertCell(0);
    var r2cell1 = row2.insertCell(1);
    var r2cell2 = row2.insertCell(2);
    r2cell0.innerHTML = "Total";
    r2cell1.innerHTML = otherPayments;
    $("#otherPaymentsTotal").text("Total: "+otherPayments);
    
    $("#label_total").text("Total: "+totalBill);
  }
  function removeItem(r) {
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("paymentTable").deleteRow(i);
    var x= payments.filter(function(value, index, arr){
        return value.id!=r.value;
    });
    payments = x;
    otherPayments=0;
    payments.forEach(element => {
      var sTotal=0
      otherPayments = otherPayments+parseInt(element.amount);
    })
    $("#otherPaymentsTotal").text("Total: "+otherPayments);
    var table = document.getElementById('paymentTable');
    table.rows[table.rows.length-1].cells[1].innerHTML = otherPayments;
  }
  function setDate() {
    var today = new Date();
    var hh = String(today.getHours()).padStart(2, '0');
    var min = String(today.getMinutes()).padStart(2, '0');
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = mm + '/' + dd + '/' + yyyy;
    var time = hh+":"+min;
    console.log(today);
    document.getElementById("receiveDate").value = today;
    document.getElementById("receiveTime").value = time;
  }
    /////////////////////////////////////////////////// Form Submit Add  

  $(function () {

      $('#jobRegister').on('submit', function (e) {

        e.preventDefault();

        $.ajax({
          type: 'post',
          url: '../controller/c_job_register.php',
          data: $('#jobRegister').serialize(),
          success: function (data) {

                if(data==0){

                  swal({
                    title: "Customer or Vehicle Duplication !",
                    text: "Buyer",
                    icon: "error",
                    button: "Ok !",
                  });

                }else{

                  swal({
                  title: "Good job !",
                  text: "Successfully Submited",
                  icon: "success",
                  button: "Ok !",
                  });
                  setTimeout(function(){ location.reload(); }, 2500);
                  
                }
              }
        });
      });
    });
  
  </script>
<!DOCTYPE html>
<html lang="en">
  <?php // Database Connection
  // Database Connection
  require "../include/config.php"; ?>
  <!-- include head code here -->
  <?php include "../include/head.php"; ?>
  <body>
    <div class="container-scroller">
      <!-- partial:../../partials/_navbar.html -->
      <!-- include nav code here -->
      <?php include "../include/nav.php"; ?>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:../../partials/_sidebar.html -->
        <!-- include sidebar code here -->
        <?php include "../include/sidebar.php"; ?>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">
              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                    <h4 class="card-title">Company Profile</h4>
                    <form class="form-sample" id="jobRegister">

                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Name</label>
                                <div class="col-md-9">
                                <input type="text" class="form-control" name="cName" placeholder ="Enter Customer Name"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Email</label>
                                <div class="col-md-9">
                                <input type="text" class="form-control" name="cName" placeholder ="Enter Customer Name"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Address</label>
                                <div class="col-md-9">
                                <textarea name="cAddress" class="form-control" id="textAreaExample" rows="4" placeholder="Enter Address"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Telephone</label>
                                <div class="col-md-9">
                                <input type="text" class="form-control" name="cTel" placeholder ="Enter Telephone"/>
                                </div>
                            </div>
                            
                          </div>
                            <div class="col-md-6" style="margin-bottom: 10px;
                              display: flex;
                              justify-content: center;
                              flex-direction: column;
                              align-items: center;">
                              <div style="margin-bottom: 10px;">
                                <div id="img-preview-default">
                                  <img src="../assets/images/company/profile.jpg">
                                </div>
                                <div id="img-preview" ></div>
                              </div>
                              <label for="email"><b>Company Logo</b></label>
                              <input type="file" id="choose-file" accept="image/*" />
                            </div>
                        </div>
                        <input type="hidden" class="form-control" name="post_job" value="post_job" />
                        <button type="submit" class="btn btn-success mr-2 fr" >Submit</button>      
                    </form>
                    
                    </div>
                </div>
              </div>
             
             
            </div>
          </div>
          <!-- content-wrapper ends -->
          <!-- partial:../../partials/_footer.html -->
          <!-- include footer coe here -->
          <?php include "../include/footer.php"; ?>
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- include footer coe here -->
    <?php include "../include/footer-js.php"; ?>

  </body>
</html>


  <script>
    /////////////////////////////////////////////////// Form Submit Add  

    $(function () {

        $('#jobRegister').on('submit', function (e) {

          e.preventDefault();

          $.ajax({
            type: 'post',
            url: '../controller/c_job_register.php',
            data: $('#jobRegister').serialize(),
            success: function (data) {

                  if(data==0){

                    swal({
                      title: "Customer or Vehicle Duplication !",
                      text: "Buyer",
                      icon: "error",
                      button: "Ok !",
                    });

                  }else{

                    swal({
                    title: "Good job !",
                    text: "Successfully Submited",
                    icon: "success",
                    button: "Ok !",
                    });
                    setTimeout(function(){ location.reload(); }, 2500);
                    
                  }
               }
          });
        });
      });
  
  </script>
  <script type="text/javascript">
      const chooseFile = document.getElementById("choose-file");
			const imgPreview = document.getElementById("img-preview");
			const imgPreviewDefault = document.getElementById("img-preview-default");
			
			var fname = "";
			var lname = "";
			var selectedSubjects = "";
			var description = "";
			var image= "";
			chooseFile.addEventListener("change", function () {
				imgPreviewDefault.style.display = "none";
				getImgData();
			});
			
			function postFormData() {
				$.post("../dashbordApi.php", {
					form: "reg_company",
					first_name: fname,
					last_name: lname,
					subjects: selectedSubjects,
					description: description,
					url: image
				},
				function(data) {
					if (data.state == 1) {
						swal("Success!", "Company registration successfull!", "success");
					} else {
						$(loader).hide();
						swal("Oops...", "Failed to register!", "error");
					}
				});
			}
			function getImgData() {
				const files = chooseFile.files[0];
				if (files) {
					const fileReader = new FileReader();
					fileReader.readAsDataURL(files);
					fileReader.addEventListener("load", function () {
					imgPreview.style.display = "block";
					imgPreview.innerHTML = '<img style='+'"width:200px;"'+'src="' + this.result + '" />';
					});    
				}
			}
      $(function() { //create a function that waits for the DOM to be ready
          smb = $('#reg_btn');
          smb.on('click', function(e) { //Capture the submit button click
                    var subjects = [];
					selectedSubjects= "";
          fname = document.getElementById('first_name').value;
          lname = document.getElementById('last_name').value;
                    // subjects = document.getElementById('subjects').value;
					description = document.getElementById('description').value;
					var fileImage= document.getElementById('choose-file').files[0];
					image= "";
					var url = document.getElementById('choose-file').value;
					var form_data = new FormData();                  
    				form_data.append('file', fileImage);
					
                    if (fname == "" || lname == "" ) {
                        swal("Missing", "Please fill all the required fields!", "warning");
                        return;
                    }
					
					
          var fd = new FormData();
					fd.append('file',fileImage);
					console.log(fd);
					$.ajax({
						form: "teacher_image_upload",
						url: '../imageUploadApi.php',
						type: 'post',
						data: fd,
						contentType: false,
						processData: false,
						success: function(response){
							if(response != 0){
								if(response.state==1){
									image= response.image;
									e.preventDefault(); //prevent the form to be sent when you click
									if(image!=""){
										postFormData();
									}
								}
								else{
									swal("Missing", "No image!", "warning");
								}
							}else{
								swal("Missing", "File not uploaded!", "warning");
							}
						},
					});
                   
                    
      });
  });
  </script>
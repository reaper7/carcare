<!DOCTYPE html>
<html lang="en">
  <?php
    // Database Connection
    require '../include/config.php';
  ?>
  <!-- include head code here -->
  <?php  include('../include/head.php');   ?>
  <body>
    <div class="container-scroller">
      <!-- partial:../../partials/_navbar.html -->
      <!-- include nav code here -->
      <?php  include('../include/nav.php');   ?>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:../../partials/_sidebar.html -->
        <!-- include sidebar code here -->
        <?php  include('../include/sidebar.php');   ?>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">
              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">User table</h4>
                    <p class="card-description fl">Dashboard >> <code>User</code> </p>
                    <button type="submit" class="btn btn-primary btn-fw fr"data-toggle="modal" data-target="#myModal" onclick="setDate()" >Create New User</button>              
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th> # </th>
                          <th> Employee Name </th>
                          <th> Email </th>
                          <th>User Level</th>
                          <th> Created By </th>
                          <th> Created Date </th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php
                          $sql=mysqli_query($conn,"SELECT * FROM user");
                          
                          $numRows = mysqli_num_rows($sql); 
                    
                          if($numRows > 0) {
                            $i = 1;
                            while($row = mysqli_fetch_assoc($sql)) {

                              $employeeName = $row['employeeName'];
                              $email  = $row['email'];
                              $userLevel   = $row['userLevel'];
                              $createdBy   = $row['createdBy'];
                              $cdate   = $row['cdate'];
                              echo ' <tr>';
                              echo ' <td>'.$i.' </td>';
                              echo ' <td>'.$employeeName.' </td>';
                              echo ' <td>'.$email.' </td>';
                              echo ' <td>'.$userLevel.' </td>';
                              echo ' <td>'.$createdBy.' </td>';
                              echo ' <td>'.$cdate.' </td>';
                              echo ' </tr>';
                              $i++;
                            }
                          }
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
             
             
            </div>
          </div>
          <!-- content-wrapper ends -->
          <!-- partial:../../partials/_footer.html -->
          <!-- include footer coe here -->
          <?php include('../include/footer.php');   ?>
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- include footer coe here -->
    <?php include('../include/footer-js.php');   ?>

  </body>
</html>

 <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
          <h4 class="modal-title">User Registation</h4>
        </div>
        <div class="modal-body">
            <div class="card">
                <div class="card-body">
                <h4 class="card-title">Create New User</h4>
                <form class="form-sample" id="userAdd">
                    <p class="card-description">Personal info</p>
                    <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                        <label class="col-sm-3 col-form-label" >Emp Name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name ="employeeName" placeholder="Enter Employee Name" required/>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                        <label class="col-sm-3 col-form-label">User Level</label>
                        <div class="col-sm-9">
                            <!-- <input type="text" class="form-control" /> -->
                            <select class="form-control" name="userLevel">
                            <option value="">--Select Level--</option>
                            <option value="SuperUser">SuperUser</option>
                            <option value="Admin">Admin</option>
                            <option value="Moderator">Moderator</option>
                            <option value="User">User</option>
                            </select>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Emp ID</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" name ="employeeId" placeholder="Enter Employee ID" required/>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Section</label>
                        <div class="col-sm-9">
                            <!-- <input class="form-control" placeholder="dd/mm/yyyy" /> -->
                            <select class="form-control" name="section">
                            <option value="">--Select Section--</option>
                            <option value="S1">S1</option>
                            <option value="S2">S2</option>
                            <option value="S3">S3</option>
                            <option value="S4">S4</option>
                            </select>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Emp Email</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" name ="email" placeholder="Enter Employee Email" required/>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Username</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name ="username" placeholder="Enter Username" required/>
                        </div>
                        </div>
                    </div>
                    </div>
                    <!-- <p class="card-description"> Address </p> -->
                    <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Date</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name ="cdate" id="cdate" placeholder="dd/mm/yyyy" required disabled/>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Password</label>
                        <div class="col-sm-9">
                            <input type="password" class="form-control" name="confirmPassword" placeholder="Enter Password" required/>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                        <label class="col-sm-3 col-form-label">User</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="createdBy" placeholder="Created By" value="
                            <?php if(isset($_SESSION['username'])) echo trim($_SESSION['username']);?>" disabled/>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Password</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="password" placeholder="Re-enter password" password/>
                        </div>
                        </div>
                    </div>
                    </div>
                    <!-- <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                            <label class="col-sm-3 col-form-label">City</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" />
                            </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Country</label>
                            <div class="col-sm-9">
                                <select class="form-control">
                                <option>America</option>
                                <option>Italy</option>
                                <option>Russia</option>
                                <option>Britain</option>
                                </select>
                            </div>
                            </div>
                        </div>
                    </div> -->
                    <input type="hidden" class="form-control" name="add_user" value="add_user" />
                    <button type="submit" class="btn btn-success mr-2 fr" >Submit</button>      
                </form>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  



  <script>
    
    function setDate() {
      var today = new Date();
      var dd = String(today.getDate()).padStart(2, '0');
      var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = today.getFullYear();

      today = mm + '/' + dd + '/' + yyyy;
      console.log(today);
      document.getElementById("cdate").value = today;
    }
  
    /////////////////////////////////////////////////// Form Submit Add  

    $(function () {

        $('#userAdd').on('submit', function (e) {

          e.preventDefault();

          $.ajax({
            type: 'post',
            url: '../controller/user.php',
            data: $('#userAdd').serialize(),
            success: function (data) {

              swal({
                title: "Good job !",
                text: "Successfully Submited",
                icon: "success",
                button: "Ok !",
                });
                setTimeout(function(){ location.reload(); }, 2500);
               }
          });

        });

      });
  
  </script>
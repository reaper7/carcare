<!DOCTYPE html>
<html lang="en">
  <?php require "../include/config.php"; ?>
  <!-- include head code here -->
  <?php include "../include/head.php"; ?>
  <body>
    <div class="container-scroller">
      <!-- partial:../../partials/_navbar.html -->
      <!-- include nav code here -->
      <?php include "../include/nav.php"; ?>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:../../partials/_sidebar.html -->
        <!-- include sidebar code here -->
        <?php include "../include/sidebar.php"; ?>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">
              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Purchase orders</h4>
                    <p class="card-description fl">Dashboard >> <code>Purchase orders</code> </p>
                    <!-- <button type="submit" class="btn btn-primary btn-fw fr"data-toggle="modal" data-target="#myModal" onclick="setDate()" >Add New Parts</button>               -->
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>P Order ID</th>
                          <th>Job id</th>
                          <th>Date</th>
                          <th>Time</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php
                      $sql = mysqli_query($conn, "SELECT *FROM purchase_order");

                      $numRows = mysqli_num_rows($sql);

                      $numRows = mysqli_num_rows($sql);

                      if ($numRows > 0) {
                        $i = 1;
                        while ($row = mysqli_fetch_assoc($sql)) {
                          $po_id = $row["po_id"];
                          $job_id = $row["job_id"];
                          $poTime = $row["poTime"];
                          $poDate = $row["poDate"];
                          echo " <tr>";
                          echo " <td>" . $i . " </td>";
                          echo " <td>" . $po_id . " </td>";
                          echo " <td>" . $job_id . " </td>";
                          echo " <td>" . $poDate . " </td>";
                          echo " <td>" . $poTime . " </td>";
                          echo " <td>";
                          // echo '<button onclick="printPo(' .
                          //   $job_id .
                          //   ')" style="width: 16px;height: 27px;margin-right: 10px;"' .
                          //   'class="btn btn-primary"' .
                          //   'data-toggle="modal"' .
                          //   'data-target="#myModal"><i class="fas fa-file-invoice-dollar" style="margin-right: 10px !important;margin: -4px;"></i></button>';
                          echo '<button onclick="viewMore(' .
                            $job_id .
                            ')" style="width: 16px;height: 27px;"' .
                            'class="btn btn-success"' .
                            'data-toggle="modal"' .
                            'data-target="#myModal"><i class="fa fa-play" style="margin-right: 10px !important;margin: -4px;"></i></button>';
                          echo " </td>";
                          echo " </tr>";
                          $i++;
                        }
                      } else {
                        echo " <tr><td colspan='10'><p style='
                        display: flex;
                        justify-content: center;'
                        >No records found</p></td></tr>";
                      }
                      ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
             
             
            </div>
          </div>
          <!-- content-wrapper ends -->
          <!-- partial:../../partials/_footer.html -->
          <!-- include footer coe here -->
          <?php include "../include/footer.php"; ?>
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- include footer coe here -->
    <?php include "../include/footer-js.php"; ?>

  </body>
</html>

 <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
          <h4 class="modal-title">Purchase Order</h4>
        </div>
        <div class="modal-body">
            <div class="card">
              <div class="card-body" id="printDiv">
                <label class="card-title text-danger" id="label_id" ></label>
                <div class="row">
                  <div class="col-md-12">
                    <label>Spare parts Ordered</label>
                    <table id="partTable" class="table table-hover" >
                      <thead>
                          <th>Part Name</th>
                          <th>Part No</th>
                          <th>Status</th>
                          <th>Quantity</th>
                          <th>U.Price</th>
                          <!-- <th style="text-align: end;" >S.total</th> -->
                      </thead>
                      <tbody id="tbodyid">
                      </tbody>
                    </table>
                  </div>
                  <div class="col-md-12 mt-4">
                    <button type="button" onclick="printPo()" class="btn btn-success mr-2 fr" >Print</button>      
                  </div>
                </div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  <script>
    var poArray = [];
    function viewMore(jId){
      $("#label_id").text("Job ID "+ jId);
      //get job array
      var resData = [];
        $.get(
            "../controller/c_purchase_order.php", {
                poSpareparts: "poSpareparts",
                jId:jId
            },
            function(data) {
                resData = data.data;
                if(resData!=undefined){
                  if(resData.length>0){
                    poArray=resData;
                    setSparepart(poArray)
                  }else{
                    poArray = [];
                  }
                }
          }
        );
    }
    function printPo(jId) {
      var divContents = document.getElementById("printDiv").innerHTML;
      var a = window.open('', '');
      a.document.write('<html>');
      a.document.write('<body >');
      a.document.write(divContents);
      a.document.write('</body></html>');
      a.document.close();
      a.print();
    }
    function setSparepart(spareparts) {
      var table = document.getElementById("partTable");
      spareparts.forEach(element => {
        var partName = element.spName;
        var spNo = element.spNo;
        var spQuantity = parseInt(element.spQuantity);
        var sTotal = parseInt(element.orderedQuantity)*parseInt(element.spPrice);
        var status = spQuantity>=parseInt(element.orderedQuantity) ? "Avialable" : "Out of stock";
        var row = table.insertRow(-1);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        var cell5 = row.insertCell(4);
        
        cell1.innerHTML = partName;
        cell2.innerHTML = spNo;
        cell3.innerHTML = status;
        cell4.innerHTML = element.orderedQuantity;
        cell5.innerHTML = element.spPrice;

        // cell4.innerHTML = "<div><input style="+"'width:50px;'"+ "class="+"'form-control'"+"placeholder='0'"+
        //                   "autocomplete="+"'off'"+"/>"+"</div>";
      });
      // $("#total").val()=totalBillParts;
    }
    function setDate() {
      var today = new Date();
      var dd = String(today.getDate()).padStart(2, '0');
      var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = today.getFullYear();

      today = mm + '/' + dd + '/' + yyyy;
      

     document.getElementById("spDate").value = today;


    }
  
    /////////////////////////////////////////////////// Form Submit Add  

    $(function () {

        $('#partsAdd').on('submit', function (e) {

          e.preventDefault();

          $.ajax({
            type: 'post',
            url: '../controller/spare_parts.php',
            data: $('#partsAdd').serialize(),
            success: function (data) {

              swal({
                title: "Good job !",
                text: "Successfully Submited",
                icon: "success",
                button: "Ok !",
                });
                setTimeout(function(){ location.reload(); }, 2500);
               }
          });

        });

      });
  
  </script>
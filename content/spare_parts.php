<!DOCTYPE html>
<html lang="en">
  <?php require "../include/config.php"; ?>
  <!-- include head code here -->
  <?php include "../include/head.php"; ?>
  <body>
    <div class="container-scroller">
      <!-- partial:../../partials/_navbar.html -->
      <!-- include nav code here -->
      <?php include "../include/nav.php"; ?>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:../../partials/_sidebar.html -->
        <!-- include sidebar code here -->
        <?php include "../include/sidebar.php"; ?>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">
              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Spare Parts table</h4>
                    <p class="card-description fl">Dashboard >> <code>Spare Parts</code> </p>
                    <button type="submit" class="btn btn-primary btn-fw fr"data-toggle="modal" data-target="#myModal" onclick="addNew()" >Add New Parts</button>              
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Part Code</th>
                          <th>Name</th>
                          <th>Category</th>
                          <th>MadeIn</th>
                          <th>MadeBy</th>
                          <th>Quantity</th>
                          <th>Price</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php
                      $sql = mysqli_query($conn, "SELECT * FROM spareparts");

                      $numRows = mysqli_num_rows($sql);

                      if ($numRows > 0) {
                        $i = 1;
                        while ($row = mysqli_fetch_assoc($sql)) {
                          $spId = $row["spId"];
                          $spName = $row["spName"];
                          $spCategory = $row["spCategory"];
                          $spCountry = $row["spCountry"];
                          $spMade = $row["spMade"];
                          $spNo = $row["spNo"];
                          $spQuantity = $row["spQuantity"];
                          $spPrice = $row["spPrice"];
                          echo " <tr>";
                          echo " <td>" . $i . " </td>";
                          echo " <td>" . $spNo . " </td>";
                          echo " <td>" . $spName . " </td>";
                          echo " <td>" . $spCategory . " </td>";
                          echo " <td>" . $spCountry . " </td>";
                          echo " <td>" . $spMade . " </td>";
                          echo " <td>" . $spQuantity . " </td>";
                          echo " <td>" . $spPrice . " </td>";
                          echo ' <td><button style="margin-right:10px;width: 16px;height: 27px;"
                              class="btn btn-success" onclick="editSparepart(' .
                            $spId .
                            ')"
                              data-toggle="modal" "
                              data-target="#myModal"><i class="fa fas fa-edit" style="
                              margin-right: 10px !important;
                              margin: -4px;"></i></button>';
                          echo '<button style="width: 16px;height: 27px;"
                              class="btn btn-danger" onclick="removeSparePart(' .
                            $spId .
                            ')" ><i class="fas fa-trash-alt" style="
                              margin-right: 10px !important;
                              margin: -4px;"></i></button> </td>';
                          echo " </tr>";
                          $i++;
                        }
                      }
                      ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
             
             
            </div>
          </div>
          <!-- content-wrapper ends -->
          <!-- partial:../../partials/_footer.html -->
          <!-- include footer coe here -->
          <?php include "../include/footer.php"; ?>
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- include footer coe here -->
    <?php include "../include/footer-js.php"; ?>

  </body>
</html>

 <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
          <h4 class="modal-title" id="titleHeading">Add S/Parts</h4>
        </div>
        <div class="modal-body">
            <div class="card">
                <div class="card-body">
                <form class="form-sample" id="partsAdd">
                    <p class="card-description">Parts info</p>
                    <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                        <label class="col-sm-3 col-form-label" >Code<i  class="text-danger">*</i></label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id ="spNo" name ="spNo" placeholder="Enter S/Parts Code" required/>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Category</label>
                        <div class="col-sm-9">
                            <!-- <input type="text" class="form-control" /> -->
                            <select class="form-control" id="spCategory" name="spCategory" required>
                            <option value="">-Select Category-</option>
                            <option value="SuperUser">Bulb</option>
                            <option value="Admin">Oil</option>
                            <option value="Moderator">Filters</option>
                            <option value="User">Coolant</option>
                            </select>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Name<i  class="text-danger">*</i></label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="spName" name ="spName" placeholder="Enter s/Part Name" required/>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                        <label class="col-sm-3 col-form-label">MadeIn</label>
                        <div class="col-sm-9">
                            <!-- <input class="form-control" placeholder="dd/mm/yyyy" /> -->
                            <select class="form-control" id="spCountry" name="spCountry">
                            <option value="">Select country</option>
                            <option value="Japan">Japan</option>
                            <option value="Thaiwan">Thaiwan</option>
                            <option value="UK">UK</option>
                            <option value="China">China</option>
                            </select>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Quantity</label>
                        <div class="col-sm-9">
                          <input type="number" class="form-control" id ="spQuantity" name ="spQuantity" placeholder="Enter Quantity" required/>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                        <label class="col-sm-3 col-form-label">MadeBy</label>
                        <div class="col-sm-9">
                          <select class="form-control" id ="spMade" name="spMade" required>
                            <option value="">None</option>
                            <option value="Toyota">Toyota</option>
                            <option value="Delo">Delo</option>
                            <option value="Honda">Honda</option>
                            <option value="Mobil">Mobil</option>
                          </select>
                        </div>
                        </div>
                    </div>
                    </div>
                    <!-- <p class="card-description"> Address </p> -->
                    <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Date</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name ="spDate" id="spDate" placeholder="dd/mm/yyyy" disabled/>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Price</label>
                        <div class="col-sm-9">
                            <input type="number" class="form-control" id="spPrice" name="spPrice" placeholder="Enter Selling Price" required/>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Cost</label>
                        <div class="col-sm-9">
                            <input type="number" class="form-control" id="spCost" name="spCost" placeholder="Enter Cost" value="" required/>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Profit</label>
                        <div class="col-sm-9">
                            <input type="number" class="form-control" id="spProfit" name="spProfit" placeholder=""/>
                        </div>
                        </div>
                    </div>
                    </div>
                    
                    <input type="hidden" class="form-control" id="sparepartId" name="sparepartId" value="" />
                    <input type="hidden" class="form-control" id="form_state" name="add_sp" value="add_sp" />
                    <button type="submit" id="btnSubmit" class="btn btn-success mr-2 fr" >Submit</button>      
                    <button type="submit" id="btnUpdate" class="btn btn-success mr-2 fr" >Update</button>      
                </form>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  



<script>
  var sparepartArray = [];
  //call when page loading
  $( document ).ready(function() {
      getSpares();
  });
  function getSpares(){
    //get job array
    var resData = [];
      $.get(
          "../controller/c_spare_parts.php", {
              get_spareparts: "get_spareparts"
          },
          function(data) {
              resData = data.data;
              if(resData!=undefined){
                if(resData.length>0){
                  sparepartArray=resData;
                }else{
                  sparepartArray = [];
                }
              }
        }
      );
  }
  function editSparepart(id) {
      $("#form_state").val("update");
      $("#btnUpdate").show();
      $("#btnSubmit").hide();
      $("#titleHeading").text("Update Sparepart");
      $("#sparepartId").val("update_part");
      $("#sparepartId").val(id);
      var selectedPart={}
      sparepartArray.forEach(element => {
        if(id==element.spId){
          selectedPart=element;
        }
      });
      console.log(selectedPart);
      $("#spCategory").val(selectedPart.spCategory);
      $("#spCost").val(selectedPart.spCost);
      $("#spCountry").val(selectedPart.spCountry);
      $("#spDate").val(selectedPart.spDate);
      $("#spId").val(selectedPart.spId);
      $("#spMade").val(selectedPart.spMade);
      $("#spName").val(selectedPart.spName);
      $("#spNo").val(selectedPart.spNo);
      $("#spPrice").val(selectedPart.spPrice);
      $("#cNic").val(selectedPart.ownerNic);
      $("#spProfit").val(selectedPart.spProfit);
      $("#spQuantity").val(selectedPart.spQuantity);
      
  }
  function addNew() {
    $("#form_state").val("add_sp");
    $("#btnSubmit").show();
    $("#btnUpdate").hide();
    $('#partsAdd')[0].reset();
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0');
    var yyyy = today.getFullYear();

    today = mm + '/' + dd + '/' + yyyy;
    console.log(today);
    document.getElementById("spDate").value = today;
  }
  function removeSparePart(spId) {
    swal({
        title: "Are you sure?",
        text: "Sparepart will be removed!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          //delete request
          $.ajax({
              type: "post",
              url: "../controller/c_spare_parts.php",
              data: {act: 'delete_sparepart',spId: spId},
              success: function (data) {
                swal({
                  title: "Good job !",
                  text: "Successfully deleted",
                  icon: "success",
                  button: "Ok !",
                });
                setTimeout(function () {
                  location.reload();
                }, 2500);
              },
            });
        }
      });
  }
////Form Submit Add  and update ,same function to update and insert

    $(function () {

        $('#partsAdd').on('submit', function (e) {

          e.preventDefault();

          $.ajax({
            type: 'post',
            url: '../controller/c_spare_parts.php',
            data: $('#partsAdd').serialize(),
            success: function (data) {

              swal({
                title: "Good job !",
                text: "Successfully Submited",
                icon: "success",
                button: "Ok !",
                });
                setTimeout(function(){ location.reload(); }, 2500);
               }
          });

        });

      });
  
  </script>
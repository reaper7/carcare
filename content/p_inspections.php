<!DOCTYPE html>
<html lang="en">
  <?php require "../include/config.php"; ?>
  <!-- include head code here -->
  <?php include "../include/head.php"; ?>
  <body>
    <div class="container-scroller">
      <!-- partial:../../partials/_navbar.html -->
      <!-- include nav code here -->
      <?php include "../include/nav.php"; ?>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:../../partials/_sidebar.html -->
        <!-- include sidebar code here -->
        <?php include "../include/sidebar.php"; ?>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">
              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Inspections</h4>
                    <p class="card-description fl">
                      Dashboard >> <code>Inspections</code>
                    </p>
                   
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th> # </th>
                          <th>Customer</th>
                          <th>Phone</th>
                          <!-- <th>Address</th> -->
                          <th>V/No</th>
                          <th>V/Brand</th>
                          <th>J/Type</th>
                          <th>Status</th>
                          <!-- <th>Description </th>
                          <th>T_Description </th> -->
                          <th>Job</th>
                          <th>Action</th>
                        </tr>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $sql = mysqli_query(
                          $conn,
                          "SELECT * FROM cliant c 
                            INNER JOIN vehicle v ON c.cNic = v.ownerNic
                            INNER JOIN job j ON v.vNo = j.vNo
                            ORDER BY j.jobId DESC"
                        );

                        $numRows = mysqli_num_rows($sql);
                        $jobArray = [];
                        if ($numRows > 0) {
                          $indexRows = 1;
                          while ($row = mysqli_fetch_assoc($sql)) {
                            $cName = $row["cName"];
                            $cEmail = $row["cEmail"];
                            $technicalDescription =
                              $row["technicalDescription"];
                            $telephone = $row["cTel"];
                            $cAddress = str_replace('"',"", $row["cAddress"]);
                            $vNo = $row["vNo"];
                            $brandName = $row["brandName"];
                            $cDescription = $row["cDescription"];
                            $jobType = $row["jobType"];
                            $receiveTime = $row["receiveTime"];
                            $jobId = $row["jobId"];
                            $jstatus = $row["jstatus"];
                            $proof = $row["proof"];
                            $jobAssign = $row["jobAssign"];

                            echo " <tr>";
                            echo " <td>" . $indexRows . " </td>";
                            echo " <td>" . $cName . " </td>";
                            echo " <td>" . $telephone . " </td>";
                            // echo ' <td>'.$address.' </td>';
                            echo " <td>" . $vNo . " </td>";
                            echo " <td>" . $brandName . " </td>";
                            echo " <td>" . $jobType . " </td>";
                            echo " <td>" . $jstatus . " </td>";
                            // echo " <td>" . $cDescription . " </td>";
                            // echo " <td>" . $technicalDescription . " </td>";
                            echo " <td>" .
                              $jobId .
                              " </td>";

                            echo ' <td><button onclick="selectRow(' .
                              $jobId .
                              ')" style="width: 16px;height: 27px;"
                              class="btn btn-success"
                              data-toggle="modal" "
                              data-target="#myModal"><i class="fa fas fa-edit"" style="
                              margin-right: 10px !important;
                              margin: -7px;"></i></button>';
                            echo '<button onclick="viewJob(' .
                              $jobId .
                              ')" style="width: 16px;height: 27px;margin-left:10px;"' .
                              'class="btn btn-primary"' .
                              'data-toggle="modal"' .
                              'data-target="#myModal"><i class="fas fa-eye" style="margin-right: 10px !important;margin: -8px;"></i></button>';

                            echo " </tr>";
                            $indexRows++;
                          }
                        }
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- content-wrapper ends -->
          <!-- partial:../../partials/_footer.html -->
          <!-- include footer coe here -->
          <?php include "../include/footer.php"; ?>
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- include footer coe here -->
    <?php include "../include/footer-js.php"; ?>
  </body>
</html>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <h4 class="modal-title" id="titleJob">Process Inspection</h4>
      </div>
      <div class="modal-body">
        <div class="card">
          <div class="card-body">
            <!-- <h4 class="card-title">Process Inspection</h4> -->
            <div class="card-body" id="job" style= "display:none;">
              <form class="form-sample" id="jobRegister">
                <label class="card-title text-danger" id="label_id" ></label>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Name</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="cName" placeholder ="Enter Customer Name"/>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Brand</label>
                        <div class="col-sm-9">
                          <select id="brandName" class="form-control">
                            <option>-Select Brand-</option>
                            <option value="BMW">BMW</option>
                            <option value="Audi">Audi</option>
                            <option value="Honda">Honda</option>
                            <option value="Toyota">Toyota</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Email</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="cEmail" placeholder ="Enter Email"/>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Body Type</label>
                        <div class="col-sm-9">
                        <select id="bodyType" class="form-control">
                            <option>-Select Type-</option>
                            <option value="Car">Car</option>
                            <option value="Double Cab">Double Cab</option>
                            <option value="SUV">SUV</option>
                            <option value="Truck">Truck</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Address</label>
                        <div class="col-sm-9">
                          <!-- <input type="text" class="form-control" name="address" placeholder ="Enter Address"/> -->
                          <textarea name="cAddress" class="form-control" id="cAddress" rows="4" placeholder="Enter Address"></textarea>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Description</label>
                        <div class="col-sm-9">
                          <!-- <input type="text" class="form-control" name="description" placeholder ="Enter Description"/> -->
                          <textarea class="form-control" name="cDescription" id="cDescription" rows="4" placeholder="Description"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Telephone</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="cTel" placeholder ="Enter Telephone"/>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Year</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="modelYear" placeholder ="Enter Model Year"/>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Vehicle No</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="vNo" name="vNo" placeholder ="Enter V/No"/>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Date</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" Blocked id="receiveDate" name="receiveDate" placeholder ="dd/mm/yyyy"/>
                          <input type="hidden" Blocked class="form-control" id="receiveTime" name="receiveTime"/>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label" >NIC</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="cNic" placeholder ="Enter Customer NIC"/>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Odometer</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="odometer" placeholder ="Enter Odometer Reading"/>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Next</label>
                        <div class="col-sm-9">
                          <select id="jobType" class="form-control">
                            <option value="Inspection">Inspection</option>
                            <option value="Parts">Parts</option>
                            <option value="P/O">P/O</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>

                  <input type="hidden" class="form-control" id="post_job" value="post_job" />
                  <button type="submit" id="submitBtn" class="btn btn-success mr-2 fr" >Submit</button>      
              </form>
              
            </div>
            <div id="inspection">
              <form
                class="form-sample"
                id="styleForm"
                enctype="multipart/form-data"
              >
              <div id="serviceSection">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Received Time</label>
                      <div class="col-sm-9">
                        <input
                          id="receiveTime"
                          value="<?php echo $receiveTime; ?>"
                          class="form-control"
                          placeholder="00.00"
                          autocomplete="off"
                        />
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Delivery Time(hours)</label>
                      <div class="col-sm-9">
                        <input
                          type="text"
                          id="deliverTime"
                          class="form-control"
                          placeholder="00.00"
                          autocomplete="off"
                        />
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label"
                        >Fault diagnosed</label
                      >
                      <div class="col-sm-9">
                        <select id="proof" class="form-control">
                          <option value="Yes">Yes</option>
                          <option value="No">No</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <!-- <h6 class="modal-title">Job Status</h6> -->
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Status</label>
                      <div class="col-sm-9">
                        <select
                          class="form-control"
                          id="statusDropdown"
                          onchange="setStatus(this.value)"
                        >
                          <option value="Pending">Pending</option>
                          <option value="Processing">Processing</option>
                          <option value="Canceled">Canceled</option>
                          <option value="Completed">Completed</option>
                          <option value="Held">Held</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                <div id="status_div" style="display: none">
                  <!-- <div class="row">
                    <div class="col-md-12">
                      <h4 class="card-title">Reason</h4>
                    </div>
                  </div> -->
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-6 col-form-label"
                          >Reason for holding or cancelling</label
                        >
                        <div class="col-sm-9">
                          <textarea
                            class="form-control"
                            id="reason"
                            rows="2"
                            id="reason"
                          ></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Assign job</label>
                      <div class="col-sm-9">
                        <select id="jobAssign" class="form-control">
                          <option value="Electrician">Electrician</option>
                          <option value="Mechanic">Mechanic</option>
                          <option value="AC">AC</option>
                          <option value="Tinker">Tinker</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <!-- <label class="col-sm-4 col-form-label">Spare parts</label> -->
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Type</label>
                      <div class="col-sm-9">
                        <select
                          class="form-control"
                          id="jobType"
                        >
                          <option value="service_repair">Service/ Repair</option>
                          <option value="spare_parts">Spare parts</option>
                          <option value="service_spareparts">
                            Service and Spare parts
                          </option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label"
                        >Technical description</label
                      >
                      <div class="col-sm-9">
                        <textarea
                          class="form-control"
                          rows="2"
                          id="technicalDescription"
                        ></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php  ?>
                <div id="spare_part_div" style="display: none">
                  <div class="row" style="display: flex; justify-content: space-between;">
                    <h4 class="card-title">Add spare parts</h4>
                    <label class="text-danger" id="total"></label>
                  </div>
                  <div class="row">
                    <div class="col-md-12" style="display: flex; justify-content: flex-end;">
                      <div class="col-md-7">
                      <input type="number" placeholder="Enter Quantity" class="custom-select" style=" width: 200px;
                      height: 30px;" id="spQuantity" >
                      <input list="dlist" placeholder="Select Part" class="custom-select" style=" width: 300px;
                        height: 30px;" onchange="selectPart(event)" id="dinput" name="dinput">
                        <datalist id="dlist" >
                            <?php
                            $sql = mysqli_query(
                              $conn,
                              "SELECT * FROM spareparts"
                            );
                            $numRows = mysqli_num_rows($sql);
                            $spareparts = [];
                            if ($numRows > 0) {
                              $index = 1;
                              while ($row = mysqli_fetch_assoc($sql)) {
                                $spId = $row["spId"];
                                $spName = $row["spName"];
                                $spQuantity = $row["spQuantity"];
                                $spPrice = $row["spPrice"];
                                $spNo = $row["spNo"];

                                array_push(
                                  $spareparts,
                                  (object) [
                                    "spId" => $spId,
                                    "spName" => $spName,
                                    "spQuantity" => $spQuantity,
                                    "spPrice" => $spPrice,
                                    "spNo" => $spNo,
                                  ]
                                );
                                $index++;
                              }
                            }
                            foreach ($spareparts as $ele) {
                              echo '<option id=" ' .
                                $ele->spId .
                                '" value="' .
                                $ele->spName .
                                '" data-value="' .
                                $ele->spId .
                                '">' .
                                $ele->spName .
                                "</option>";
                            }
                            ?>
                        </datalist>
                        <?php  ?>
                        <button type="button" onclick="addRow(event)" class="btn btn-success mr-2 fr" style=" margin-right: 18px !important;">
                          Add
                        </button>
                      </div>
                      
                    </div>
                  </div>
                
                  <div class="row">
                    <div class="col-md-12">
                      <!-- <label class="col-sm-4 col-form-label">Spare parts</label> -->
                      <table id="partTable" class="table table-hover" >
                        <thead>
                            <th>Part Name</th>
                            <th>Part No</th>
                            <th>Status</th>
                            <th>Quantity</th>
                            <th>U.Price</th>
                            <th style="text-align: end;">S.total</th>
                            <th>Action</th>
                        </thead>
                        <?php  ?>
                        <tbody id="tbodyid">
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <input
                  type="hidden"
                  class="form-control"
                  id="jobId"
                  id="jobId"
                />
                <div class="row" style="display: flex;justify-content: flex-end;">
                  <button type="button" id="backBtn" style="margin-top: 12px; display:none; margin-right: 10px;" onclick="setServiceDivVisible()" class="btn btn-secondary ">
                    Back
                  </button>
                  <button type="submit" class="btn btn-success mr-2 fr" style="margin-top: 12px; margin-right: 18px !important;">
                    Save
                  </button>
                  <button type="button" onclick="setInspType()" id="btnAddPart" class="btn btn-success mr-2 fr" style="margin-top: 12px; margin-right: 18px !important;">
                    Sparepart
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">
          Close
        </button>
      </div>
    </div>
  </div>
</div>

<script>
  var jobArray = [];
  var selectedSpareparts = [];
  var addedSpareparts = [];
  var totalBill=0;
  var jId = 0;
  var seletedPart= { spId: "", spNo: "", spName: "", spQuantity: "", spPrice: "",orderedQuantity :""};
  $( document ).ready(function() {
      getJobs();
  });
  function getJobs(){
    //get job array
    var resData = [];
      $.get(
          "../controller/c_inspections.php", {
              inspectionlist: "inspectionlist"
          },
          function(data) {
              resData = data.data;
              if(resData!=undefined){
                if(resData.length>0){
                  jobArray=resData;
                }else{
                  jobArray = [];
                }
              }
        }
      );
  }
  /////////////////////////////////////////////////// Form Submit Add
  $(function () {
    $("#styleForm").on("submit", function (e) {
      e.preventDefault();
      var spart = selectedSpareparts;
      var jobId = $('#jobId').val();
      var deliverTime = $('#deliverTime').val();
      var jobAssign = $('#jobAssign').val();
      var proof = $('#proof').val();
      var receiveTime = $('#receiveTime').val();
      var jstatus = $('#statusDropdown').val();
      var jobType = $('#jobType').val();
      var reason = $('#reason').val();
      var technicalDescription = $('#technicalDescription').val();
      var job = JSON.stringify([{
                  'jobId' : jobId,
                  'receiveTime': receiveTime,
                  'technicalDescription': technicalDescription,
                  'jobType'	: jobType	,
                  'jstatus'	: jstatus	,
                  'deliverTime': deliverTime,
                  'jobAssign': jobAssign,
                  'proof': proof,
                  'reason': reason
                }]);
      $.ajax({
        type: "post",
        url: "../controller/c_inspections.php",
        data:{ jobdata: job, spare_parts: spart,form:"postJob" },
        success: function (data) {
          swal({
            title: "Good job !",
            text: "Successfully Submited",
            icon: "success",
            button: "Ok !",
          });
          setTimeout(function () {
            location.reload();
          }, 2500);
        },
      });
    });
  });
  //set jobid value 
  function selectRow(val) {
    $('#inspection').show();
    $('#job').hide();
    $('#titleJob').text("Process Inspection");
    clearTable();
    var resData = [];
    document.getElementById("jobId").value = val;
      jId = val;
      $.get(
          "../controller/c_inspections.php", {
              sparepartlist: "sparepartlist",
              id : jId
          },
          function(data) {
              resData = data.data;
              if(resData!=undefined){
                if(resData.length>0){
                  setSparepart(resData);
                }
              }
        }
      );
      
    var selectedJob={}
    jobArray.forEach(element => {
      if(val==element.jobId){
        selectedJob=element;
      }
    });
    $("#jobAssign").val(selectedJob.jobAssign);
    $("#technicalDescription").val(selectedJob.technicalDescription);
    $("#proof").val(selectedJob.proof);
    $("#statusDropdown").val(selectedJob.jstatus);
  }
  function viewJob(jid) {
    
    $('#inspection').hide();
    $('#job').show();
    $('#label_id').text("Job id: "+jid);
    $('#titleJob').text("Job Info");
    setData(jid);
  }
  function setData(id) {
      $( "#vNo" ).prop( "disabled", true );
      $( "#cNic" ).prop( "disabled", true );
      $( "#receiveDate" ).prop( "disabled", true );
      $( "#submitBtn" ).prop( "disabled", true );
      $("#post_job").val("edit");
      var selectedJob={}
      jobArray.forEach(element => {
        if(id==element.jobId){
          selectedJob=element;
        }
      });
      console.log(selectedJob);
      $("#vNo").val(selectedJob.vNo);
      $("#cName").val(selectedJob.cName);
      $("#cDescription").val(selectedJob.cDescription);
      $("#jobType").val(selectedJob.jobType);
      $("#cEmail").val(selectedJob.cEmail);
      $("#cTel").val(selectedJob.cTel);
      $("#cAddress").val(selectedJob.cAddress);
      $("#brandName").val(selectedJob.brandName);
      $("#jobAssign").val(selectedJob.jobAssign);
      $("#reason").val(selectedJob.reason);
      $("#cNic").val(selectedJob.ownerNic);
      $("#modelYear").val(selectedJob.modelYear);
      $("#bodyType").val(selectedJob.bodyType);
      $("#odometer").val(selectedJob.odometer);
      $("#receiveDate").val(selectedJob.receiveDate);
  }
  function clearTable(){
    $("#spQuantity").val("");
    $("#dinput").val("");
    totalBill=0;
    selectedSpareparts = [];
    addedSpareparts= [];
    $("#total").text("Total: "+totalBill);
    removeTableBody();
    setServiceDivVisible();
  }
  function selectPart(s) {
    var spareparts = JSON.parse('<?php echo json_encode($spareparts); ?>');
    var value = $('#dinput').val();
    value = $('#dlist [value="' + value + '"]').data('value');
    var duplicatePart=false;
    
    selectedSpareparts.forEach(element => {
      if(element.spId == value){
        duplicatePart = true;
      }
    });
    spareparts.forEach(element => {
      if(element.spId == value){
        seletedPart= { spId: "", spNo: "", spName: "", spQuantity: "", spPrice: "",orderedQuantity :0};
        seletedPart.spId = element.spId;
        seletedPart.spName = element.spName;
        seletedPart.spQuantity = element.spQuantity;
        seletedPart.spPrice = element.spPrice;
        seletedPart.spNo = element.spNo;
        if(!duplicatePart){
          selectedSpareparts.push(seletedPart);
        }
      }
    });
    // $('#suggestions').val(value);
    
  }
  function setSparepart(spareparts) {
    selectedSpareparts=spareparts;
    var table = document.getElementById("partTable");
    totalBill=0;
    spareparts.forEach(element => {
      var partName = element.spName;
      var spNo = element.spNo;
      var spId = element.spId;
      var spQuantity = parseInt(element.spQuantity);
      var sTotal = parseInt(element.orderedQuantity)*parseInt(element.spPrice);
      var status = spQuantity>=parseInt(element.orderedQuantity) ? "Avialable" : "Out of stock";
      totalBill = totalBill+sTotal;
      var row = table.insertRow(-1);
      var cell1 = row.insertCell(0);
      var cell2 = row.insertCell(1);
      var cell3 = row.insertCell(2);
      var cell4 = row.insertCell(3);
      var cell5 = row.insertCell(4);
      var cell6 = row.insertCell(5);
      var cell7 = row.insertCell(6);
      
      cell1.innerHTML = partName;
      cell2.innerHTML = spNo;
      cell3.innerHTML = status;
      cell4.innerHTML = element.orderedQuantity;
      cell5.innerHTML = element.spPrice;
      cell6.innerHTML = sTotal;
      // cell4.innerHTML = "<div><input style="+"'width:50px;'"+ "class="+"'form-control'"+"placeholder='0'"+
      //                   "autocomplete="+"'off'"+"/>"+"</div>";
      cell7.innerHTML = "<button type="+"'button'"+"class="+"'btn btn-danger mr-2 fr'"+ "value="+"'"+spId+"'"+
                        "onclick="+"'"+"removeItem(this)"+"'"+">Delete </button>";
    });
    // $("#total").val()=totalBill;
    var row2 = table.insertRow(-1);
    var r2cell0 = row2.insertCell(0);
    var r2cell1 = row2.insertCell(1);
    var r2cell2 = row2.insertCell(2);
    var r2cell3 = row2.insertCell(3);
    var r2cell4 = row2.insertCell(4);
    var r2cell5 = row2.insertCell(5);
    var r2cell6 = row2.insertCell(6);
    r2cell0.innerHTML = "Total";
    r2cell5.innerHTML = totalBill;
    $("#total").text("Total: "+totalBill);
  }
  function removeTableBody(){
    var tableHeaderRowCount = 1;
    var table = document.getElementById('partTable');
    var rowCount = table.rows.length;
    for (var i = tableHeaderRowCount; i < rowCount; i++) {
        table.deleteRow(tableHeaderRowCount);
    }
  }
  function addRow(e) {
    totalBill=0;
    var selectedSpare= false;
    var value = $('#dinput').val();
    value = $('#dlist [value="' + value + '"]').data('value');
    var spQuantityOrdered = $('#spQuantity').val();
    selectedSpareparts.forEach(item => {
      if(item.spId==value){
        item.orderedQuantity=parseInt(item.orderedQuantity)+parseInt(spQuantityOrdered);
      }
    });
    if(value=="" || value==undefined){
        swal({
          title: "Please select spare part !",
          text: "Spare part",
          icon: "error",
          button: "Ok !",
        });
      return;
    }
    if(spQuantityOrdered=="" || parseInt(spQuantityOrdered)<=0){
      swal({
        title: "Please enter Quantity !",
        text: "Quantity",
        icon: "error",
        button: "Ok !",
      });
      return;
    }
    this.removeTableBody();
    var table = document.getElementById("partTable");
    selectedSpareparts.forEach(element => {
        var status = element.spQuantity>=spQuantityOrdered ? "Avialable" : "Out of stock"
        var sTotal = parseInt(element.orderedQuantity)*parseInt(element.spPrice);
        totalBill = totalBill+sTotal;
        var row = table.insertRow(-1);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        var cell5 = row.insertCell(4);
        var cell6 = row.insertCell(5);
        var cell7 = row.insertCell(6);

        cell1.innerHTML = element.spName;
        cell2.innerHTML = element.spNo;
        cell3.innerHTML = status;
        cell4.innerHTML = element.orderedQuantity;
        cell5.innerHTML = element.spPrice;
        cell6.innerHTML = sTotal;
        cell7.innerHTML = "<button type="+"'button'"+"class="+"'btn btn-success mr-2 fr'"+ "value="+"'"+element.spId+"'"+
                          "onclick="+"'"+"removeItem(this)"+"'"+">Delete </button>";
    });
    var row2 = table.insertRow(-1);
    var r2cell0 = row2.insertCell(0);
    var r2cell1 = row2.insertCell(1);
    var r2cell2 = row2.insertCell(2);
    var r2cell3 = row2.insertCell(3);
    var r2cell4 = row2.insertCell(4);
    var r2cell5 = row2.insertCell(5);
    var r2cell6 = row2.insertCell(6);
    r2cell0.innerHTML = "Total";
    r2cell5.innerHTML = totalBill;
    $("#total").text("Total: "+totalBill);
    $("#spQuantity").val(0);
    $("#dinput").val("");
  }
  function removeItem(r) {
  swal({
      title: "Are you sure?",
      text: "Sparepart will be removed!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $("#spQuantity").val(0);
        $("#dinput").val("");
        var i = r.parentNode.parentNode.rowIndex;
        document.getElementById("partTable").deleteRow(i);
        var x= selectedSpareparts.filter(function(value, index, arr){
            return value.spId!=r.value;
        });
        selectedSpareparts = x;
        totalBill=0;
        selectedSpareparts.forEach(element => {
          var sTotal=0;
          sTotal= parseInt(element.orderedQuantity)*parseInt(element.spPrice);
          totalBill=totalBill+sTotal;
        })
        $("#total").text("Total: "+totalBill);
        console.log("r.value"+r.value);
        console.log("totalBill"+totalBill);
        var table = document.getElementById('partTable');
        table.rows[table.rows.length-1].cells[5].innerHTML = totalBill;
        //delete request
        $.ajax({
            type: "post",
            url: "../controller/c_inspections.php",
            data: {act: 'delete_sparepart',jid: jId, partid:r.value},
            success: function (data) {
              swal({
                title: "Good job !",
                text: "Successfully deleted",
                icon: "success",
                button: "Ok !",
              });
              // setTimeout(function () {
              //   location.reload();
              // }, 2500);
            },
          });
      }
    });
  }
  function setServiceDivVisible() {
    $("#btnAddPart").show();
    $("#backBtn").hide();
    document.getElementById("spare_part_div").style.display = "none";
    document.getElementById("serviceSection").style.display = "block";
  }
  function setInspType() {
    $("#btnAddPart").hide();
      document.getElementById("spare_part_div").style.display = "block";
      document.getElementById("serviceSection").style.display = "none";
      document.getElementById("backBtn").style.display = "inline";
  }
  function setStatus(value) {
    if (value == "Canceled" || value == "Held") {
      document.getElementById("status_div").style.display = "block";
    } else {
      document.getElementById("status_div").style.display = "none";
    }
  }
</script>

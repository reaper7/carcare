<!DOCTYPE html>
<html lang="en">
  <?php
    // Database Connection
    require '../include/config.php';
  ?>
  <!-- include head code here -->
  <?php  include('../include/head.php');   ?>
  <body>
    <div class="container-scroller">
      <!-- partial:../../partials/_navbar.html -->
      <!-- include nav code here -->
      <?php  include('../include/nav.php');   ?>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:../../partials/_sidebar.html -->
        <!-- include sidebar code here -->
        <?php  include('../include/sidebar.php');   ?>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">
              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Inspections</h4>
                    <p class="card-description fl">
                      Dashboard >> <code>Active Inspections </code>
                    </p>
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th> # </th>
                          <th>Customer</th>
                          <th>Email </th>
                          <th>Telephone</th>
                          <th>Address</th>
                          <th>V/No</th>
                          <th>V/Brand</th>
                          <th>Description </th>
                          <th>Job</th>
                          <th>Action</th>
                        </tr>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          $sql=mysqli_query($conn,"SELECT * FROM cliant c 
                            INNER JOIN vehicle v ON c.cNic = v.ownerNic
                            INNER JOIN job j ON v.vNo = j.vNo
                            WHERE j.jobType='Inspection' && j.jstatus='Processing'
                            ORDER BY c.cName");
                          
                          $numRows = mysqli_num_rows($sql); 
                    
                          if($numRows > 0) {
                            $i = 1;
                            while($row = mysqli_fetch_assoc($sql)) {

                              $customer  = $row['cName'];
                              $customerEmail   = $row['cEmail'];
                              $telephone = $row['cTel'];
                              $address = $row['cAddress'];
                              $vNo = $row['vNo'];
                              $brandName = $row['brandName'];
                              $cDescription = $row['cDescription'];
                              $nextJob = $row['jobType'];
                              $receiveTime = $row['receiveTime'];
                              $jobId  = $row['jobId'];
                              echo ' <tr>';
                              echo ' <td>'.$i.' </td>';
                              echo ' <td>'.$customer.' </td>';
                              echo ' <td>'.$customerEmail.' </td>';
                              echo ' <td>'.$telephone.' </td>';
                              echo ' <td>'.$address.' </td>';
                              echo ' <td>'.$vNo.' </td>';
                              echo ' <td>'.$brandName.' </td>';
                              echo ' <td>'.$cDescription.' </td>';
                              echo ' <td>'.$nextJob.' </td>';
                              echo ' <td><button style="width: 16px;height: 27px;"
                              class="btn btn-primary"
                              data-toggle="modal" "
                              data-target="#myModal"><i class="fa fa-play" style="
                              margin-right: 10px !important;
                              margin: -4px;"></i></button> </td>';
                              echo ' </tr>';
                              $i++;
                            }
                          }
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- content-wrapper ends -->
          <!-- partial:../../partials/_footer.html -->
          <!-- include footer coe here -->
          <?php include('../include/footer.php');   ?>
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- include footer coe here -->
    <?php include('../include/footer-js.php');   ?>
  </body>
</html>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <h4 class="modal-title">Process Inspection</h4>
      </div>
      <div class="modal-body">
        <div class="card">
          <div class="card-body">
            <!-- <h4 class="card-title">Process Inspection</h4> -->
            <form
              class="form-sample"
              id="styleForm"
              enctype="multipart/form-data"
            >
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Received Time</label>
                    <div class="col-sm-9">
                      <input
                        id="receiveTime"
                        value="<?php echo $receiveTime; ?>"
                        class="form-control"
                        placeholder="00.00"
                        autocomplete="off"
                      />
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Delivery Time</label>
                    <div class="col-sm-9">
                      <input
                        name="deliverTime"
                        class="form-control"
                        placeholder="00.00"
                        autocomplete="off"
                      />
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label"
                      >Proof of fault (diagnosed)</label
                    >
                    <div class="col-sm-9">
                      <select name="proof" class="form-control">
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <!-- <h6 class="modal-title">Job Status</h6> -->
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Status</label>
                    <div class="col-sm-9">
                      <select
                        class="form-control"
                        id="statusDropdown"
                        name="jstatus"
                        onmousedown="this.value='';"
                        onchange="setStatus(this.value);"
                      >
                        <option value="Pending">Pending</option>
                        <option value="Processing">Processing</option>
                        <option value="Canceled">Canceled</option>
                        <option value="Completed">Completed</option>
                        <option value="Held">Held</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div id="status_div" style="display: none">
                <!-- <div class="row">
                  <div class="col-md-12">
                    <h4 class="card-title">Reason</h4>
                  </div>
                </div> -->
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group row">
                      <label class="col-sm-6 col-form-label"
                        >Reason for holding or cancelling</label
                      >
                      <div class="col-sm-9">
                        <textarea
                          class="form-control"
                          id="reason"
                          rows="2"
                          name="reason"
                        ></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Assign job</label>
                    <div class="col-sm-9">
                      <select name="jobAssign" class="form-control">
                        <option value="Electrician">Electrician</option>
                        <option value="Mechanic">Mechanic</option>
                        <option value="AC">AC</option>
                        <option value="Tinker">Tinker</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <!-- <label class="col-sm-4 col-form-label">Spare parts</label> -->
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Type</label>
                    <div class="col-sm-9">
                      <select
                        class="form-control"
                        id="servType"
                        name="servType"
                        onmousedown="this.value='';"
                        onchange="setInspType(this.value);"
                      >
                        <option value="service">Service/ Repair</option>
                        <option value="spare_parts">Spare parts</option>
                        <option value="service_spare_parts">
                          Service and Spare parts
                        </option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div id="spare_part_div" style="display: none">
                <div class="row">
                  <div class="col-md-12">
                    <h4 class="card-title">Add spare parts</h4>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Part name</label>
                      <div class="col-sm-9">
                        <select class="form-control">
                          <option>ECU</option>
                          <option>10A Fuse</option>
                          <option>Brake Pad</option>
                          <option>Socket</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Quantity</label>
                      <div class="col-sm-9">
                        <input
                          class="form-control"
                          placeholder="0"
                          autocomplete="off"
                        />
                      </div>
                    </div>
                  </div>
                  <div
                    class="col-md-4"
                    style="
                      display: flex;
                      align-items: center;
                      justify-content: flex-start;
                    "
                  >
                    <button
                      type="submit"
                      style="margin-top: 12px"
                      class="btn btn-success mr-2 fr"
                    >
                      Add
                    </button>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <!-- <label class="col-sm-4 col-form-label">Spare parts</label> -->
                    <table class="table table-hover">
                      <thead>
                        <tr>
                          <th>Index</th>
                          <th>Part Name</th>
                          <th>Part No</th>
                          <th>Status</th>
                          <th>Quantity</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>01</td>
                          <td>Brake Pad</td>
                          <td>BR120</td>
                          <td>Available</td>
                          <td>2</td>
                          <td>
                            <label class="badge badge-danger">Pending</label>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label"
                      >Technical description</label
                    >
                    <div class="col-sm-9">
                      <textarea
                        class="form-control"
                        id="technical_description"
                        rows="2"
                        name="technicalDescription"
                      ></textarea>
                    </div>
                  </div>
                </div>
              </div>

              <input
                type="hidden"
                class="form-control"
                name="postins"
                value="postins"
              />
              <input
                type="hidden"
                class="form-control"
                name="jobId"
                value=" <?php echo $jobId; ?>"
              />
              <button type="submit" class="btn btn-success mr-2 fr">
                Save
              </button>
            </form>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">
          Close
        </button>
      </div>
    </div>
  </div>
</div>

<script>
  /////////////////////////////////////////////////// Form Submit Add

  $(function () {
    $("#styleForm").on("submit", function (e) {
      e.preventDefault();

      var data = new FormData($("#styleForm")[0]);

      $.ajax({
        type: "post",
        url: "../controller/c_pending_inspections.php",
        cache: false,
        contentType: false,
        processData: false,
        data: data,
        success: function (data) {
          swal({
            title: "Good job !",
            text: "Successfully Submited",
            icon: "success",
            button: "Ok !",
          });
          setTimeout(function () {
            location.reload();
          }, 2500);
        },
      });
    });
  });

  function viewData(val) {
    document.getElementById("receiveTime").value = val;
  }
  function setInspType(value) {
    if (value == "service") {
      document.getElementById("spare_part_div").style.display = "none";
    } else {
      document.getElementById("spare_part_div").style.display = "block";
    }
  }
  function setStatus(value) {
    if (value == "canceled" || value == "held") {
      document.getElementById("status_div").style.display = "block";
    } else {
      document.getElementById("status_div").style.display = "none";
    }
  }
</script>

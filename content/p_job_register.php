<!DOCTYPE html>
<html lang="en">
  <?php require "../include/config.php"; ?>
  <!-- include head code here -->
  <?php include "../include/head.php"; ?>
  <body>
    <div class="container-scroller">
      <!-- partial:../../partials/_navbar.html -->
      <!-- include nav code here -->
      <?php include "../include/nav.php"; ?>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:../../partials/_sidebar.html -->
        <!-- include sidebar code here -->
        <?php include "../include/sidebar.php"; ?>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">
              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                  <h4 class="card-title">Job Register table</h4>
                    <p class="card-description fl">Dashboard >> <code>Job Register</code> </p>
                    <button type="submit" class="btn btn-primary btn-fw fr"data-toggle="modal" data-target="#myModal" onclick="createNew()" >Create New Job</button>              
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th> # </th>
                          <th>Customer</th>
                          <th>Email </th>
                          <th>Telephone</th>
                          <th>V/No</th>
                          <th>V/Brand</th>
                          <th>Description </th>
                          <th>Job</th>
                          <th>Action</th>
                        </tr>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $sql = mysqli_query(
                          $conn,
                          "SELECT * FROM cliant c 
                          INNER JOIN vehicle v ON c.cNic = v.ownerNic
                          INNER JOIN job j ON v.vNo  = j.vNo ORDER BY j.jobId DESC"
                        );

                        $numRows = mysqli_num_rows($sql);
                        $jobArray = [];
                        if ($numRows > 0) {
                          $i = 1;
                          while ($row = mysqli_fetch_assoc($sql)) {
                            $cName = $row["cName"];
                            $jobId = $row["jobId"];
                            $cEmail = $row["cEmail"];
                            $cTel = $row["cTel"];
                            $cAddress = $row["cAddress"];
                            $vNo = $row["vNo"];
                            $brandName = $row["brandName"];
                            $cDescription = $row["cDescription"];
                            $jobType = $row["jobType"];
                            array_push(
                              $jobArray,
                              (object) [
                                "jobId" => $jobId,
                                "vNo" => $vNo,
                                "ownerNic" => $row["ownerNic"],
                                "cName" => $cName,
                                "cDescription" => $cDescription,
                                "jobType" => $jobType,
                                "cEmail" => $cEmail,
                                "cTel" => $cTel,
                                "cAddress" => $cAddress,
                                "brandName" => $brandName,
                                "jstatus" => $row["jstatus"],
                                "deliverTime" => $row["deliverTime"],
                                "jobAssign" => $row["jobAssign"],
                                "reason" => $row["reason"],
                                "ownerNic" => $row["ownerNic"],
                                "modelYear" => $row["modelYear"],
                                "bodyType" => $row["bodyType"],
                                "odometer" => $row["odometer"],
                                "receiveDate" => $row["receiveDate"],
                              ]
                            );
                            echo " <tr>";
                            echo " <td>" . $i . " </td>";
                            echo " <td>" . $cName . " </td>";
                            echo " <td>" . $cEmail . " </td>";
                            echo " <td>" . $cTel . " </td>";
                            echo " <td>" . $vNo . " </td>";
                            echo " <td>" . $brandName . " </td>";
                            echo " <td>" . $cDescription . " </td>";
                            echo " <td>" . $jobType . " </td>";
                            echo '<td><button onclick="editRow(' .
                              $jobId .
                              ')" style="margin-left:10px; width: 16px;height: 27px;"
                              class="btn btn-success"
                              data-toggle="modal" "
                              data-target="#myModal"><i class="fa fas fa-edit" style="
                              margin: -7px; font-size: 16px;
                              margin-right: 10px !important;
                              "></i></button>';
                            echo " </td>";
                            echo " </tr>";
                            $i++;
                          }
                        }
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
             
             
            </div>
          </div>
          <!-- content-wrapper ends -->
          <!-- partial:../../partials/_footer.html -->
          <!-- include footer coe here -->
          <?php include "../include/footer.php"; ?>
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- include footer coe here -->
    <?php include "../include/footer-js.php"; ?>

  </body>
</html>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog modal-lg">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <h4 class="modal-title">Job Register</h4>
      </div>
      <div class="modal-body">
          <div class="card">
              <div class="card-body">
              <h4 class="card-title">Create Job</h4>
              <form class="form-sample" id="jobRegister">
                  <p class="card-description">Job info</p>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Name</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="cName" name="cName" placeholder ="Enter Customer Name" required/>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Brand</label>
                        <div class="col-sm-9">
                          <select name="brandName" id="brandName" class="form-control" required>
                            <option>-Select Brand-</option>
                            <option value="BMW">BMW</option>
                            <option value="Audi">Audi</option>
                            <option value="Honda">Honda</option>
                            <option value="Toyota">Toyota</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Email</label>
                        <div class="col-sm-9">
                          <input type="email" class="form-control" id="cEmail" name="cEmail" placeholder ="Enter Email"/>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Body Type</label>
                        <div class="col-sm-9">
                        <select name="bodyType" id="bodyType" class="form-control" required>
                            <option>-Select Type-</option>
                            <option value="Car">Car</option>
                            <option value="Double Cab">Double Cab</option>
                            <option value="SUV">SUV</option>
                            <option value="Truck">Truck</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Address</label>
                        <div class="col-sm-9">
                          <!-- <input type="text" class="form-control" name="address" placeholder ="Enter Address"/> -->
                          <textarea name="cAddress" class="form-control" id="cAddress" rows="4" placeholder="Enter Address"></textarea>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Description</label>
                        <div class="col-sm-9">
                          <!-- <input type="text" class="form-control" name="description" placeholder ="Enter Description"/> -->
                          <textarea class="form-control" name="cDescription" id="cDescription" rows="4" placeholder="Description" required></textarea>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Telephone</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="cTel" name="cTel" placeholder ="Enter Telephone" required/>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Year</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="modelYear" name="modelYear" placeholder ="Enter Model Year"/>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Vehicle No</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="vNo" name="vNo" placeholder ="Enter V/No" required/>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Date</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" Blocked id="receiveDate" name="receiveDate" placeholder ="dd/mm/yyyy"/>
                          <input type="hidden" Blocked class="form-control" id="receiveTime" name="receiveTime"/>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label" >NIC</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="cNic" name="cNic" placeholder ="Enter Customer NIC" required/>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Odometer</label>
                        <div class="col-sm-9">
                          <input type="number" class="form-control" id="odometer" name="odometer" placeholder ="Enter Odometer Reading" required/>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Job Type</label>
                        <div class="col-sm-9">
                          <select name="jobType" id="jobType" class="form-control" required>
                            <option value="Inspection">Inspection</option>
                            <option value="Service">Service</option>
                            <option value="Body Wash">Body Wash</option>
                            <option value="Parts">Parts</option>
                            <option value="P/O">P/O</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>

                  <input type="hidden" class="form-control" id="post_job" name="post_job" value="new" />
                  <input type="hidden" class="form-control" id="update_job" name="update_job" value="" />
                  <button type="submit" class="btn btn-success mr-2 fr" >Submit</button>      
              </form>
              
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    
  </div>
</div>

  <script>
    var jobArray = <?php echo json_encode($jobArray); ?>;
    var formStatus= "new";
    function editRow(id) {
      $( "#vNo" ).prop( "disabled", true );
      $( "#cNic" ).prop( "disabled", true );
      $( "#receiveDate" ).prop( "disabled", true );
      formStatus = "edit"
      $("#post_job").val("edit");
      $("#update_job").val(id);
      console.log(jobArray);
      var selectedJob={}
      jobArray.forEach(element => {
        if(id==element.jobId){
          selectedJob=element;
        }
      });
      $("#vNo").val(selectedJob.vNo);
      $("#cName").val(selectedJob.cName);
      $("#cDescription").val(selectedJob.cDescription);
      $("#jobType").val(selectedJob.jobType);
      $("#cEmail").val(selectedJob.cEmail);
      $("#cTel").val(selectedJob.cTel);
      $("#cAddress").val(selectedJob.cAddress);
      $("#brandName").val(selectedJob.brandName);
      $("#jobAssign").val(selectedJob.jobAssign);
      $("#reason").val(selectedJob.reason);
      $("#cNic").val(selectedJob.ownerNic);
      $("#modelYear").val(selectedJob.modelYear);
      $("#bodyType").val(selectedJob.bodyType);
      $("#odometer").val(selectedJob.odometer);
      $("#receiveDate").val(selectedJob.receiveDate);
    }
    function createNew() {
      $('#jobRegister')[0].reset();
      formStatus = "new"
      var today = new Date();
      var hh = String(today.getHours()).padStart(2, '0');
      var min = String(today.getMinutes()).padStart(2, '0');
      var dd = String(today.getDate()).padStart(2, '0');
      var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = today.getFullYear();

      today = mm + '/' + dd + '/' + yyyy;
      var time = hh+":"+min;
      console.log(today);
      document.getElementById("receiveDate").value = today;
      document.getElementById("receiveTime").value = time;
    }
    /////////////////////////////////////////////////// Form Submit Add  

    $(function () {

        $('#jobRegister').on('submit', function (e) {
          console.log(formStatus);
          e.preventDefault();
          if(formStatus =="new"){
            $.ajax({
              type: 'post',
              url: '../controller/c_job_register.php',
              data: $('#jobRegister').serialize(),
              success: function (data) {

                    if(data==0){

                      swal({
                        title: "Customer or Vehicle Duplication !",
                        text: "Buyer",
                        icon: "error",
                        button: "Ok !",
                      });

                    }else{

                      swal({
                      title: "Good job !",
                      text: "Successfully Submited",
                      icon: "success",
                      button: "Ok !",
                      });
                      setTimeout(function(){ location.reload(); }, 2500);
                      
                    }
                }
            });
          }else{
            $( "#vNo" ).prop( "disabled", false );
            $( "#cNic" ).prop( "disabled", false );
            $( "#receiveDate" ).prop( "disabled", false );
            $.ajax({
              type: 'post',
              url: '../controller/c_job_register.php',
              data: $('#jobRegister').serialize(),
              success: function (data) {
                      swal({
                      title: "Success !",
                      text: "Successfully Updated",
                      icon: "success",
                      button: "Ok !",
                      });
                      setTimeout(function(){ location.reload(); }, 2500);
                }
            });
          }
        });
      });
  
  </script>